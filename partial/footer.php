<!-- section footer -->
  <!-- <footer>
    <div class="footer" style="position: relative;">
      <img alt="Logo Kiducation" id="pattern" src="images/pattern11.png" style="position: absolute; bottom: -26px; left: -8px;" width="350" />
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <img alt="Logo Kiducation" src="images/logowhite.png" width="200" />
            <div class="footer-address">
              Keep updated by our Workshops and Programs. 
            </div>
          
            <span>Follow Us: </span>
            <div class="instagram">
              instagram : <a href="https://www.instagram.com/kiducation.id/" target="_blank">@kiducation</a>
            </div>

            <div class="facebook">
              facebook : <a href="https://www.facebook.com/kiducation.id/" target="_blank">@kiducation</a>
            </div>
          </div>


          <div class="col-sm-4 col-sm-push-4" style="text-align: right;">
            <div class="footer-title">About kiducation</div>
            <ul>
              <li><a href="aboutus.php">about us</a></li>
              <li><a href="#">Term and conditions</a></li>
              <li><a href="faq.php">FAQ</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer> -->
  <!-- section footer -->

  <!--START: Footer B-->
      <footer class="footer" style="position: relative; overflow: hidden;">
        <img alt="Logo Kiducation" class="footer-pattern" id="pattern" src="images/pattern11.png" style="position: absolute; bottom: -26px; left: -8px; transition: all 0.5s;" width="350" />
        <div class="footer-top" style="padding: 2rem;">
          <div class="container">
            <div class="text-center">
              <img alt="Logo Kiducation" src="images/logowhite.png" width="200" style="display: block; margin: 0 auto; margin-bottom: 2rem" />
              <a href="https://www.facebook.com/kiducation.id/" target="_blank" class="text-dark d-inline-block mx-1">
                <img alt="facebook" src="images/icon/facebook-logo-button.svg" width="30" style="margin: 0 3px;" />
              </a>
              <a href="https://www.instagram.com/kiducation.id/" target="_blank" class="text-dark d-inline-block mx-1">
                <img alt="instagram" src="images/icon/instagram-logo.svg" width="30" style="margin: 0 3px;"/>
              </a>
              <a href="https://www.youtube.com/channel/UC2awQUYTyaNOX6heE5iIxDw" target="_blank" class="text-dark d-inline-block mx-1">
                <img alt="youtube" src="images/icon/youtube-symbol.svg" width="30" style="margin: 0 3px;"/>
              </a>
            </div>
          </div>
        </div>
        <div class="footer-bottom py-4">
          <div class="container text-center">
            <div class="d-block mb-5">
              <small class="mx-2 text-uppercase-custom">
                <a href="aboutus.php" class="text-dark">About Us</a>
              </small>
              <small class="mx-2 text-uppercase-custom">
                <a href="faq.php" class="text-dark">Faq</a>
              </small>
              <small class="mx-2 text-uppercase-custom">
                <a href="blog.php" class="text-dark">Blog</a>
              </small>
            </div>
          </div>
        </div>
      </footer>
      <!--END: Footer B-->