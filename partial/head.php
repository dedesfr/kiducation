	<meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>kiducation</title>
  <link rel="shortcut icon" href="images/kidu-square.png" type="image/png" />
  <link rel="stylesheet" href="dist/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" href="dist/fancybox3.5/jquery.fancybox.css">
  <link rel="stylesheet" href="stylesheets/index.css">
  <link rel="stylesheet" href="dist/animatecss/animate.min.css">
  <link href="https://fonts.googleapis.com/css?family=Hammersmith+One" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>