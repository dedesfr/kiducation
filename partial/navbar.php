<nav class="navbar navbar-default navbar-fixed-top shadow-sm" style="background-color: #fff !important; padding: 10px 0;">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">
        <img src="images/logokidu.png" width="200" style="margin-top: -9px;">
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="aboutus.php">about us</a></li>
        <li><a href="activities.php">upcoming workshops</a></li>
        <li><a href="pastworkshop.php">past workshops</a></li>
        <li><a href="organize.php">organize a workshop</a></li>
        <li><a href="blog.php">blog</a></li>
        <li><a href="getintouch.php">get in touch</a></li>
        <?php
          if($idUser != ""){
            ?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?php echo "Hi. " . $fullname; ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="top: 5rem; border-radius: 0; text-align: center; border-color: transparent; padding: 10px; border-radius: 8px;">
                  <ul style="margin: 0; padding: 0; list-style: none; text-align: left;">
                    <li  style="padding: 5px;">
                      <a class="dropdown-item a-none" href="checkpayment.php">Check Order</a>
                    </li>
                    <li  style="padding: 5px;">
                      <a class="dropdown-item a-none" href="changePassword.php">change password</a>
                    </li>
                    <li  style="padding: 5px;">
                      <a class="dropdown-item a-none" href="php-scripts/logout.php">log out</a> 
                    </li>
                  </ul>
              </li>
        <?php }
          else {
            ?>
            <li><a href="account.php">my account</a></li>
            
          <?php } ?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>