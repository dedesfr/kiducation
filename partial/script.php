<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="dist/bootstrap/js/bootstrap.min.js"></script>
<!-- google maps -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAtCa03_4E99XlF75_Nr512E_W1tQfmqwE&callback=myMap"></script>
<!-- multimodal -->
<script src="dist/multi-step-modal/multi-step-modal.js"></script>
<!-- sweetalert -->
<script src="dist/sweetalert/sweetalert.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="dist/fancybox3.5/jquery.fancybox.js"></script>
<script src="dist/viewportchecker/viewportchecker-1.8.8.min.js"></script>
<!--A jQuery plugin for adding animation based on viewport-->

<script src="js/main.js"></script>
    <!--Custom / plugin js from author-->