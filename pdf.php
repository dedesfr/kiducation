<?php
session_start();
include("base/koneksi.php");

$count = 0;

if(isset($_SESSION['fullname'])){
  $fullname = $_SESSION['fullname'];
  $idUser = $_SESSION['idUser'];
  $email = $_SESSION['email'];
  $phone = $_SESSION['phone'];
}
if (isset($_POST['submit'])) {
    $order_id = $_POST['checktype'];
}
$orderQ = mysqli_query($con, "SELECT * FROM tr_order WHERE id = $order_id");
$orderResult = mysqli_fetch_array($orderQ);

$orderNo = $orderResult['no_order'];
$orderTotal = $orderResult['total'];
$workshop_id = $orderResult['workshop_id'];

$workshopQ = mysqli_query($con, "SELECT * FROM tr_workshop WHERE w_id = $workshop_id");
$workshopResult = mysqli_fetch_array($workshopQ);

$date1 = new dateTime($workshopResult['w_date']); 
$date2 = new dateTime($workshopResult['w_date']);
$date3 = new dateTime($workshopResult['w_endDate']);


$workshopPrice = $workshopResult['w_price'];
$workshopName = $workshopResult['w_header'];
$workshopDate = $date1->format('l, F j Y');
$workshopTimeStart = $date2->format('g:ia');
$workshopTimeEnd = $date3->format('g:ia');
$workshopAddress = $workshopResult['w_address'];

$participantQ = mysqli_query($con, "SELECT * FROM participants WHERE order_id = $order_id");
require 'dist/fpdf/fpdf.php';

class PDF extends FPDF
{
// Page header
function Header()
{
    global $title;
    // Arial bold 15
    $this->SetFont('Arial','B',25);
    // Calculate width of title and position
    $w = $this->GetStringWidth($title)+6;
    // $this->SetX((210-$w)/2);
    // Colors of frame, background and text
    $this->SetFillColor(255, 255, 255);
    $this->SetTextColor(117, 230, 244);
    // Title
    $this->Cell(195,5,"E-TICKET",0,1,'C');
    // Line break
    $this->Ln(10);





}

function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Text color in gray
    $this->SetTextColor(128);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
}

}

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
    // order number
    $pdf->SetFont('Arial','B',15);
    $pdf->SetTextColor(117, 230, 244);
    $pdf->Cell(150,5,"ORDER NO : $orderNo",0,0,'C');
    $pdf->Ln(15);

    // workshop information
    $pdf->SetFont('Arial','',12);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->Cell(50,5,"Workshop",0,0);
    $pdf->Cell(5,5,":",0,0);
    $pdf->Cell(25,5,$workshopName,0,0);
    $pdf->Ln(15);
    $pdf->Cell(50,5,"Day, date",0,0);
    $pdf->Cell(5,5,":",0,0);
    $pdf->Cell(25,5,$workshopDate,0,0);
    $pdf->Ln(15);
    $pdf->Cell(50,5,"Time",0,0);
    $pdf->Cell(5,5,":",0,0);
    $pdf->Cell(25,5,"$workshopTimeStart - $workshopTimeEnd",0,0);
    $pdf->Ln(15);
    $pdf->Cell(50,5,"Venue",0,0);
    $pdf->Cell(5,5,":",0,0);
    $pdf->Cell(25,5,$workshopAddress,0,0);
    $pdf->Ln(20);

    // parents information
    $pdf->SetFont('Arial','B',15);
    $pdf->SetTextColor(117, 230, 244);
    $pdf->Cell(180,5,"PARENT'S INFORMATION",0,0,'C');
    $pdf->Ln(15);
    $pdf->SetFont('Arial','',12);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->Cell(50,5,"Parent's Name",0,0);
    $pdf->Cell(5,5,":",0,0);
    $pdf->Cell(25,5,$fullname,0,0);
    $pdf->Ln(15);
    $pdf->Cell(50,5,"Parent's Email",0,0);
    $pdf->Cell(5,5,":",0,0);
    $pdf->Cell(25,5,$email,0,0);
    $pdf->Ln(15);
    $pdf->Cell(50,5,"Parent's Phone",0,0);
    $pdf->Cell(5,5,":",0,0);
    $pdf->Cell(25,5,$phone,0,0);
    $pdf->Ln(7);
    $pdf->Cell(25,5,"Number",0,0);
    $pdf->Ln(20);

    // registration detail
    $pdf->SetFont('Arial','B',15);
    $pdf->SetTextColor(117, 230, 244);
    $pdf->Cell(180,5,"REGISTRATION DETAILS",0,0,'C');
    $pdf->SetFont('Arial','',12);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->Ln(15);
    $pdf->Cell(50,5,"No.",0,0);
    $pdf->Cell(90,5,"Participant Data",0,0);
    $pdf->Cell(25,5,"Registration Fee",0,0);
    $pdf->Ln(15);
    while($participantResult = mysqli_fetch_array($participantQ)) {
        $count += 1;

        $food_id = $participantResult['food_id'];

        $foodQ = mysqli_query($con, "SELECT * FROM food WHERE id = $food_id");
        $foodResult = mysqli_fetch_array($foodQ);


        $pdf->Cell(50,5,$count,0,0);
        $pdf->Cell(90,5,"Participant",0,0);
        $pdf->Cell(25,5,"Rp.$workshopPrice",0,0);
        $pdf->Ln(15);
        $pdf->Cell(50,5,"",0,0);
        $pdf->Cell(90,5,"Name : ". $participantResult['nama'] ,0,0);
        $pdf->Ln(15);
        $pdf->Cell(50,5,"",0,0);
        $pdf->Cell(25,5,"Age : ". $participantResult['umur'],0,0);
        $pdf->Ln(15);
        $pdf->Cell(50,5,"",0,0);
        $pdf->Cell(25,5,"Lunch Menu : ". $foodResult['nama'],0,0);
        $pdf->Ln(15);
    }
    
    $pdf->Cell(50,5,"TOTAL PAYMENT : Rp.$orderTotal",0,0);
$pdf->Output();
?>