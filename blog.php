<?php
session_start();
include("base/koneksi.php");
$id = "";
$id = @$_GET['id'];
$idUser = $_SESSION['idUser'];
$email = $_SESSION['email'];
if(isset($_SESSION['fullname'])){
  $fullname = $_SESSION['fullname'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php @include("partial/head.php") ?>
</head>
<body>
<?php @include("partial/navbar.php") ?>
  
 <!-- Blog list -->
<?php if($id == ""){ ?>
  <div class="kiducation">
    <div class="blog py-main" style="position: relative; background-color: #fafafa;">
    <img alt="pattern" id="pattern" src="images/pattern2.png" style="position: absolute; top: 16em; left: 0;" width="200" />
    <img alt="pattern" class="pattern-move" id="pattern" src="images/pattern12.png" style="position: absolute; top: 16em; left: 80px;" width="100" />
    <img alt="pattern" id="pattern" src="images/pattern4.png" style="position: absolute; top: 60em; right: 30px;" width="150" />
    <img alt="pattern" class="pattern-move" id="pattern" src="images/pattern14.png" style="position: absolute; bottom: -28px; left: 0;" width="200" />
      <div class="container">
        <div class="row">
          <div class="col-sm-3">
            <h1 class="hero-header" style="color: #4bc1c1;">blog</h1>
          </div>
          <div class="col-sm-9">
              <img src="images/blog-hero.jpg" class="img-responsive">
          </div>
          <!-- activity looping -->
          <?php
                $result = mysqli_query($con, "SELECT COUNT(*) FROM tr_blog");
                $r = mysqli_fetch_row($result);
                $numrows = $r[0];

                $result = mysqli_query($con, "SELECT * FROM tr_blog ORDER BY t_date DESC");
                while ($query = mysqli_fetch_assoc($result)) {
                $datee = new dateTime($query['t_date']);
                $datee = $datee->format('F j, Y');
          ?>
          <div class="col-sm-10 col-sm-offset-1" style="margin-top: 50px;">
            <div class="card-boxless mb-3">
              <div class="row">
                <div class="col-md-7 d-flex col-md-push-5">
                  <div class="card-body d-flex flex-column align-items-start">
                    <small class="d-block text-muted mb-1"><?php echo $datee; ?></small>
                    <a href="blog.php?id=<?php echo $query['t_id']; ?>" class="d-block text-dark mb-auto a-none">
                      <h3 class="card-title mb-2">
                        <?php echo $query['t_header']; ?>
                      </h3>
                      <p class="card-text" style="margin-bottom: 16px;">
                        <?php echo $query['t_subheader']; ?>
                      </p>
                    </a>
                    <a href="blog.php?id=<?php echo $query['t_id']; ?>" class="mt-2">Continue reading <i class="fal fa-angle-right"></i></a>
                  </div>
                </div>
                <div class="col-md-5 col-md-pull-7">
                  <div style="background: url(<?php echo $query['t_picture']; ?>) no-repeat center; background-size: cover; height: 250px;"></div>
                  <!-- <a href="blog.php?id=<?php echo $query['t_id']; ?>" class="d-none d-md-block w-100" style="height: 250px; background: url(<?php echo $query['t_picture']; ?>); background-size: cover;">
                  </a> -->
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
          <!-- activity looping -->

        </div>
      </div>
    </div>
  </div>
<?php } ?>
 <!-- Blog list -->

  <!-- Blog detail -->
  <?php if($id != ""){ 
  $query = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tr_blog WHERE t_id = $id"));
  $datee = new dateTime($query['t_date']);
  $datee = $datee->format('F j, Y');
  ?>
    <section class="blog-detail" style="padding: 7rem 0 !important;">
      <div class="cover" style="background: url(<?php echo $query['t_picture']; ?>) no-repeat center; background-size: cover;">
          <div class="container justify-content-center">
            <div class="cover-content text-center">
              <h1 class="cover-title animated fadeInUp delayp1"><?php echo $query['t_header']; ?></h1>
            </div>
          </div>
        </div>

        <section class="section-main font-size-lg" style="margin-top: 3rem; position: relative;">
          <img alt="Logo Kiducation" id="pattern" src="images/pattern17.png" style="position: absolute; top: 100px; right:8em;" width="150" class="display-none-xs" />
          <img alt="Logo Kiducation" id="pattern" src="images/pattern4.png" class="display-none-xs" style="position: absolute; top: 60em; right: 40px;" width="150" />
          <img alt="Logo Kiducation" id="pattern" src="images/pattern16.png" style="position: absolute; top: 30em; left: 50px;" width="250" class="hidden-xs" />
          <img alt="Logo Kiducation" id="pattern" src="images/pattern3.png" style="position: absolute; bottom: 90px; left: 10em;" width="150" class="display-none-xs" />
          <div class="container container-sm">
            <small class="d-block text-muted mb-1"><?php echo $datee; ?></small>
            <h3 style="margin-bottom: 3rem; color: #86cbba;"><?php echo $query['t_header']; ?></h3>
            <p><?php echo $query['t_content']; ?></p>
          </div>
        </section>
    </section>

    <!-- <div class="container">
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
          <div class="img-wrapper">
              <img src="<?php echo $query['t_picture']; ?>" class="img-responsive"> <span class="date"><?php echo $datee; ?></span>
          </div>
            <h3><?php echo $query['t_header']; ?></h3>
            <?php echo $query['t_content']; ?>
        </div>
      </div>
    </div> -->
  <?php } ?>
  <!-- end Blog detail -->

  <?php @include("partial/footer.php") ?>

  <script>
    // Get the modal
    var modal_login = document.getElementById('btn_login');
    var modal_register = document.getElementById('btn_register');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal_login) {
            modal_login.style.display = "none";
        }
    }
    window.onclick = function(event) {
        if (event.target == modal_register) {
            modal_register.style.display = "none";
        }
    }
    </script>

    <?php @include("partial/script.php") ?>
</body>
</html>