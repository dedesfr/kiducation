<?php
session_start();
include("base/koneksi.php");

$idUser = $_SESSION['idUser'];
$email = $_SESSION['email'];
if(isset($_SESSION['fullname'])){
  $fullname = $_SESSION['fullname'];
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php @include("partial/head.php") ?>
    <link rel="stylesheet" href="stylesheets/organize.css">

</head>
<body>
<?php @include("partial/navbar.php") ?>
  <div class="kiducation">
    <div class="organize" style="position: relative; padding-bottom: 50px; padding-top: 50px;">
    <img alt="pattern" id="pattern" src="images/pattern1234.png" style="position: absolute; top: 45px; right: 0;" width="200" />
    <img alt="pattern" id="pattern" src="images/6789.png" style="position: absolute; bottom: 0; left: -65px;" width="150" />
      <h1 class="organize-header animated fadeInUp delayp1">Organize your workshop with us</h1>
      <div class="container container-md">
        <div class="row">
          <div class="organize-content">
            <div class="col-sm-4" >
              <div class="img-wrapper" style="background: url(images/organize-1.jpg) no-repeat center; background-size: cover; height: 25rem;" ></div>
              <div class="img-caption">Birthday parties</div>
            </div>
            <div class="col-sm-4">
              <div class="img-wrapper" style="background: url(images/organize-2.jpg) no-repeat center; background-size: cover; height: 25rem;"></div>
              <div class="img-caption">school activities</div>
            </div>
            <div class="col-sm-4" >
              <div class="img-wrapper" style="background: url(images/organize-3.jpg) no-repeat center; background-size: cover; height: 25rem;"></div>
              <div class="img-caption">play dates/private workshops</div>
            </div>
          </div>
        </div>
      </div>
      <section class="organize-text animated fadeInUp delayp2" style="padding: 3rem 0; position: relative;">
      <img alt="Logo Kiducation" class="pattern-move" id="pattern" src="images/pattern3.png" style="position: absolute; top: 30px; left: 19em;" width="150" />
      <img alt="Logo Kiducation" class="pattern-hover" id="pattern" src="images/pattern17.png" style="position: absolute; top: 150px; right:20em;" width="150" />
        <div class="container container-xs">
          <div class="col-sm-12" style="margin-top: 40px; ">
            <div class="content-text">Planning to hold arts and crafts activities for your kids but too busy to organize? Worry not, Kiducation is here for you with our Workshop Partnership program!</div>
            <h4 class="content-text strong">How does “Workshop Partnership” work?</h4>
            <div class="content-text">Kiducation is available to organize arts and crafts workshops for any occasions such as birthday parties, community events, playdates, and school activities. Our partnership starts with the discussion and agreement about workshop concepts, location, and price. We will prepare all materials and instructors needed. On the day of the event, we will organize the workshop according to your preferences.</div>
          </div>
        </div>
      </section>

      <section>
        <div class="container container-xs">
          <div class="row">
            <div class="form-organize col-sm-12 animated fadeInUp delayp3" style="margin-top: 40px;">
              <label style="font-size: 18px; color: #3bb6bb; margin-bottom: 40px;">Contact Form</label>
              <form action="#" method="post" class="form-signin">
                <div class="form-group">
                  <label>Subject :</label>
                  <input type="text" id="Fullname" class="form-control" placeholder="Subject" name="subject" required>
                </div>
                <div class="form-group">
                  <label>Name :</label>
                  <input type="text" id="Fullname" class="form-control" placeholder="Fullname" name="email" required="">
                </div>
                <div class="form-group">
                  <label>Email :</label>
                  <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="phone" required>
                </div>
                <div class="form-group">
                  <label>Phone Number :</label>
                  <input type="text" id="inputPassword" class="form-control" placeholder="Phone Number" name="password" required="">
                </div>
                <div class="form-group">
                  <label>Message :</label>
                  <textarea class="form-control" rows="5" id="comment" required style="resize: none;"></textarea>
                </div>
                <button class="btn-register" type="submit">Send Message</button>
              </form><!-- /form -->
            </div>
          </div>
        </div>
      </section>
  
        </div>
      </div>
    </div>
  </div>

  <?php @include("partial/footer.php") ?>

  <script>
    // Get the modal
    var modal_login = document.getElementById('btn_login');
    var modal_register = document.getElementById('btn_register');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal_login) {
            modal_login.style.display = "none";
        }
    }
    window.onclick = function(event) {
        if (event.target == modal_register) {
            modal_register.style.display = "none";
        }
    }
    </script>

    <script>
      $(document).ready(function() {
  $('.vp-fadeinleft').viewportChecker({
      classToAdd: 'visible animated fadeInLeft',
      offset: 100
  });   
  $('.vp-fadeinright').viewportChecker({
      classToAdd: 'visible animated fadeInRight',
      offset: 100
  });  
  $('.vp-fadein').viewportChecker({
      classToAdd: 'visible animated fadeIn',
      offset: 100
  });
  $('.vp-fadeindown').viewportChecker({
      classToAdd: 'visible animated fadeInDown',
      offset: 100
  });
  $('.vp-fadeinup').viewportChecker({
      classToAdd: 'visible animated fadeInUp',
      offset: 100
  });
  $('.vp-slideinleft').viewportChecker({
    classToAdd: 'visible animated slideInLeft',
    offset: 100
  });  
  $('.vp-slideinright').viewportChecker({
    classToAdd: 'visible animated slideInRight',
    offset: 100
  });
  $('.vp-zoomin').viewportChecker({
    classToAdd: 'visible animated zoomIn',
    offset: 100
  });
  $('.vp-zoomindown').viewportChecker({
    classToAdd: 'visible animated zoomInDown',
    offset: 100
  });  
  $('.vp-rotatein').viewportChecker({
    classToAdd: 'visible animated rotateIn',
    offset: 100
  });  
  $('.vp-slideindown').viewportChecker({
    classToAdd: 'visible animated slideInDown',
    offset: 100
  });
  $('.parallax-window').parallax({parallax: 'scroll'});

}); 
    </script>
    <?php @include("partial/script.php") ?>
</body>
</html>