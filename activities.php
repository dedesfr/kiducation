<?php
session_start();
include("base/koneksi.php");
date_default_timezone_set("Asia/Jakarta");
$id = "";
$id = @$_GET['id'];

if(isset($_SESSION['fullname'])){
  $fullname = $_SESSION['fullname'];
  $idUser = $_SESSION['idUser'];
  $email = $_SESSION['email'];
  $phone = $_SESSION['phone'];
}

if (isset($_POST['submit'])) {
  
  $nama = $_POST['nama'];
  $umur = $_POST['umur'];

  foreach ($nama as $key => $value) {
    
  }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php @include("partial/head.php") ?>  
    <script>
      $(document).ready(function(e){

        var html = '<p /><div>Participant\'\s Name: <input type="text" name="nama" id="childnama"/> <br> Participant\'\s Age: <input type="number" name="nama" id="childumur"/> <br> <a href="#" id="remove" class="btn btn-danger">x</a></div>';

        $('#add').click(function(e){
          $('#container').append(html);
        });

        $('#container').on('click', '#remove', function(e){
          $(this).parent('div').remove();
        });


      });
    </script>
    <style>
    .m-progress-bar {
        min-height: 1em;
        background: #c12d2d;
        width: 5%;
    }

    .img-thumbnails {
      position: relative;
      height: 176px;
      width: 100%;
      overflow: hidden;
    }
    .img-thumbanils img {
      height: 100%;
      width: 100%;
    }
    .overlay {
      position: absolute;
      bottom: 100%;
      left: 0;
      right: 0;
      background-color: rgba(0, 0, 0, 0.6);
      overflow: hidden;
      width: 100%;
      height:0;
      transition: .5s ease;
    }

    .img-thumbnails:hover .overlay {
      bottom: 0;
      height: 100%;
    }

    .text {
      color: white;
      font-size: 20px;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      -ms-transform: translate(-50%, -50%);
      text-align: center;
    }
    </style>

</head>
<body>
<?php @include("partial/navbar.php") ?>

  <!-- Workshop list -->
  <?php if($id == ""){ ?>
    <div class="kiducation">
      <div class="container">
        <div class="upcoming py-main">
          <div class="row">
            <div class="col-sm-12" style="margin-bottom: 40px; position: relative;">
              <img alt="pattern" class="pattern-move" id="pattern" src="images/pattern18.png" style="position: absolute; top: 0;" width="100" />
              <img alt="pattern" class="pattern-hover" id="pattern" src="images/pattern10.png" style="position: absolute; bottom: 0; right: 0;" width="50" />
              <h1 class="upcoming-title">upcoming workshop</h1>
            </div>
            <!-- activity looping -->
              <?php
                $result = mysqli_query($con, "SELECT COUNT(*) FROM tr_workshop");
                $r = mysqli_fetch_row($result);
                $numrows = $r[0];

                $result = mysqli_query($con, "SELECT * FROM tr_workshop WHERE status = 'active' ORDER BY w_lastDate");
                while ($query = mysqli_fetch_assoc($result)) {
                $start_date = new dateTime($query['w_date']);
                $start_date = $start_date->format('l, j F Y');
                $today = date("Y-m-d");
                $end = new dateTime($query['w_lastDate']);
                $end = $end->format('Y-m-d');
                $ts1 = strtotime($today);
                $ts2 = strtotime($end);
                $ts3 = $ts1 - $ts2;
              ?>
                <div class="col-sm-3 col-xs-12" style="margin-bottom: 2rem;">
                  <a href="activities.php?id=<?php echo $query['w_id']; ?>" class="a-none">
                    <div class="card-boxless" >
                      <div class="img-thumbnails" style="background: url(<?php echo $query['w_picture']; ?>) no-repeat center; background-size: cover; position: relative;">
                        <!-- <img src="<?php echo $query['w_picture']; ?>" alt="<?php echo $query['w_header']; ?>" class="img-responsive"> -->
                        <?php if ($ts3 < 0) { ?>
                          <div class="no-overlay">
                            <div class="text"></div>
                          </div>
                        <?php } else { ?>
                          <div class="overlay">
                            <div class="text">Workshop Finished</div>
                          </div>
                        <?php } ?>
                        
                      </div>
                      <div class="card-body">
                        <h5 class="card-title img-title mb-0 text-truncate-twoline"><?php echo $query['w_header']; ?></h5>
                        <p style="margin-bottom: 19px; color: #000;">for <?php echo $query['w_minAge']; ?> years old</p>
                        <!-- <p class="card-text img-caption"><?php echo $query['w_subheader']; ?></p> -->
                        <p style="color: #000; margin-bottom: 5px;"><?php echo $start_date; ?></p>
                        <p class="text-truncate" style="color: #000; margin-bottom: 5px;"><?php echo $query['w_address']; ?></p>
                        <!--  <p>Language: <?php echo $query['w_language']; ?></p> -->
                        <p style="color: #000; margin-bottom: 5px;">Rp. <?php echo $query['w_price']; ?></p>
                        <a href="activities.php?id=<?php echo $query['w_id']; ?>" class="btn btn-register" style="margin-top: 10px;">more information</a>
                        <!-- <?php if ($ts3 < 0) { ?>
                        <?php } else { ?>
                          <h3>Workshop Finish</h3>
                        <?php } ?> -->
                        <!-- <?php
                          if (isset($_SESSION['fullname'])) { ?>
                            <?php } else { ?>
                            <a href="account.php" class="btn btn-register" style="margin-top: 10px;">more information</a>
                        <?php } ?> -->
                      </div>
                    </div>
                  </a>
                </div>              
              <?php } ?>
            <!-- activity looping -->
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
  <!-- end Workshop list -->

  <!-- workshop detail -->
  <?php if($id != ""){ 
  $_SESSION['workshop_id_temp'] = $id;
  $query = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tr_workshop WHERE w_id = $id"));
  $lat = $query['w_lat'];
  $len = $query['w_len'];
  $datee = new dateTime($query['w_date']);
  $datee = $datee->format('l, j F Y g:ia');
  $datee2 = new dateTime($query['w_endDate']);
  $datee2 = $datee2->format('g:ia');
  $datee3 = new dateTime($query['w_lastDate']);
  $datee3 = $datee3->format('l, j F Y g:ia');
  ?>
      <!--Start: Cover - Static-->
      <div class="cover" style="background: url(<?php echo $query['w_picture']; ?>) no-repeat center; background-size: cover; position: relative; margin-top: 70px;">
        <div class="container justify-content-center">
        </div>
      </div>

      <!--End: Cover - Static-->
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-1 detail-activities">
            <div class="img-wrapperaaa">
              <!-- <img src= <?php echo $query['w_picture']; ?> class="img-responsive"> -->
              <div style="margin: 25px 0;">
                <?php
                  if (isset($_SESSION['fullname'])) { ?>
                    <!-- <button class="btn btn-default" data-toggle="modal" data-target="#demo-modal-3">Show</button> -->
                    <a href="order.php?workshop=<?php echo $id; ?>" class="btn btn-register">Order</a>
                    <?php } else { ?>
                    <a href="account.php" class="btn btn-register">Register / Login</a>
                <?php } ?>
              </div>
            </div>
            <div class="detail-header" style="margin-top: 40px;">
              <div class="detail-title"><?php echo $query['w_header']; ?></div>
              <!-- <div class="detail-caption-bold">“Once upon a time, there lived a lonely tree, Flora, who really wanted to have a lot of flower friends to accompany him during day and night.”</div> -->
              <div class="detail-caption-light"><?php echo $query['w_subheader']; ?></div>
            </div>

            <div class="divider"></div>

            <div class="detail-information">
              <div class="row">
                <div class="col-sm-3">
                  <div class="detail-information-title text-muted font-size-sm">Start Date:</div>
                  <div class="detail-information-answer font-size-xl"><?php echo $datee; ?> - <?php echo $datee2; ?></div>
                </div>
                <div class="col-sm-3">
                  <div class="detail-information-title text-muted font-size-sm">Address:</div>
                  <div class="detail-information-answer font-size-xl"><?php echo $query['w_address']; ?></div>
                </div>
                <div class="col-sm-3">
                  <div class="detail-information-title text-muted font-size-sm">Class Size:</div>
                  <div class="detail-information-answer font-size-xl"><?php echo $query['w_classSize']; ?></div>
                </div>
                <div class="col-sm-3">
                  <div class="detail-information-title text-muted font-size-sm">Minimum Age:</div>
                  <div class="detail-information-answer font-size-xl"><?php echo $query['w_minAge']; ?></div>
                </div>
              </div>
              <div class="row" style="margin-top: 10px;">
                <div class="col-sm-3">
                  <div class="detail-information-title text-muted font-size-sm">Level</div>
                  <div class="detail-information-answer font-size-xl"><?php echo $query['w_level']; ?></div>
                </div>
                <div class="col-sm-3">
                  <div class="detail-information-title text-muted font-size-sm">Language:</div>
                  <div class="detail-information-answer font-size-xl"><?php echo $query['w_language']; ?></div>
                </div>
                <div class="col-sm-3">
                  <div class="detail-information-title text-muted font-size-sm">Price:</div>
                  <div class="detail-information-answer font-size-xl">Rp. <?php echo $query['w_price']; ?></div>
                </div>
                <div class="col-sm-3">
                  <div class="detail-information-title text-muted font-size-sm">Last date registration:</div>
                  <div class="detail-information-answer font-size-xl"><?php echo $datee3; ?></div>
                </div>
              </div>
            </div>

            <div class="divider"></div>

            <?php echo $query['w_content']; ?>

              <div class="divider"></div>

            <!-- <div class="detail-information-willyoulearn">
              <div class="detail-caption-bold">WHAT WILL YOU LEARN?</div>
              <div class="detail-caption-light">1. Participants will learn how to make a recycled pot and DIY flower frames.</div>
              <div class="detail-caption-light">2. As a part of this workshop, participants will also play games in order to bond as a team and to practice their cognitive and psychomotor skills.</div>
            </div>

            <div class="divider"></div>

            <div class="detail-information-willyoulearn">
              <div class="detail-caption-bold">WHAT IS THE PRICE INCLUSIVE OF?</div>
              <div class="detail-caption-light">1. Workshop materials</div>
              <div class="detail-caption-light">2. Lunch package from The Barn Restaurant</div>
              <div class="detail-caption-light">3. Certificate of participation</div>
            </div>

            <div class="divider"></div>

            <div class="detail-information-willyoulearn">
              <div class="detail-caption-bold">WHAT DO YOU NEED TO BRING?</div>
              <div class="detail-caption-light">1. Please wear comfortable clothes, shoes, and socks! Some of the activities will require participants to move a lot and play at the indoor playground.</div>
              <div class="detail-caption-light">2. As a preventive action, please bring a change of clothes in case learners' clothes are dirty from making the crafts.</div>
              <div class="detail-caption-light">3.Please bring your own water bottle.</div>
            </div>
  
            <div class="divider"></div> -->

            <div class="detail-information-willyoulearn">
              <div class="detail-caption-bold">FAQ</div>
              <div class="detail-caption-light"><strong>Q</strong>: What would be the children's lunch menu?</div>
              <div class="detail-caption-light"><strong>A</strong>: Lunch will be provided by Kiducation. At the re-registration, there would be some lunch choices and the participants are able to choose depending on their preference.</div>
              <br>
              <div class="detail-caption-light"><strong>Q</strong>: What if my children have an allergic / certain health conditions?</div>
              <div class="detail-caption-light"><strong>A</strong>: Please inform us if your children have an allergic or certain health condition that needs to be paid attention to.</div>
              <br>
              <div class="detail-caption-light"><strong>Q</strong>: Do guardians or parents need to wait for the participants?</div>
              <div class="detail-caption-light"><strong>A</strong>: To ensure participants' safety, once participants arrive at the venue of the event, every parents or guardian are obliged to fill in the Attendance List and take Guardian Card. After that, parents or guardian can wait at venue of the event, or leave and pick up the participants once they are finished.</div>
              <br>
              <div class="detail-caption-light"><strong>Q</strong>: What if I'm late to pick up my child?</div>
              <div class="detail-caption-light"><strong>A</strong>: Kiducation will ensure that each participant is picked up by their own parents or guardian according to the Attendance List. However, if the parents or guardians are late, we can assure that we would watch the participants and provide some activities to keep them busy until you come to pick them up.</div>
              <br>
              <div class="detail-caption-light"><strong>Q</strong>: Can I get my refund back if I am not able to make it last minute?</div>
              <div class="detail-caption-light"><strong>A</strong>: No, we’re sorry</div>
              <br>
              <div class="detail-caption-light"><strong>Q</strong>: Can I get my friend to take over if I am not able to make it last minute?</div>
              <div class="detail-caption-light"><strong>A</strong>: Yes. However, please kindly inform us for us to make any necessary arrangement.</div>
            </div>

            <div class="divider"></div>

            <div class="detail-information-willyoulearn">
              <div class="detail-caption-bold">TERMS & CONDITIONS</div>
              <div class="detail-caption-bold">Payment Policy</div>
              <div class="detail-caption-light">1. Please make payment within 24 hours after your registration, otherwise the registration will be cancelled.</div>
              <br>
              <div class="detail-caption-bold">Refund Policy</div>
              <div class="detail-caption-light">1. All payments are refundable.</div>
              <div class="detail-caption-light">2. However, if you are unable to attend the paid workshop participation, you can reschedule it to attend the next session. To do this please notify us via e-mail <a href="mailto: admin@kiducation.id">admin@kiducation.id</a> at least 3 days before the scheduled workshop.</div>
              <br>
              <div class="detail-caption-bold">Workshop Cancellation</div>
              <div class="detail-caption-light">1. If the workshop is cancelled regarding to unexpected events by the organizer, we will fully refund your payment.</div>
            </div>

            <div class="col-sm-12" style="margin-top: 20px;">
              <div id="googleMap" style="width:100%;height:400px;"></div>
            </div>
            <a target="_blank" class="btn btn-primary" style="margin-top: 20px;" href= <?php echo "https://maps.google.com/maps?q=". $lat ."," . $len . "&zoom=12&mapmode=streetview&daddr=". $lat ."," . $len . "" ?> >Open in Maps</a>
          </div>
        </div>
      </div>
  <?php } ?>
  <!-- end workshop detail -->

  <?php @include("partial/footer.php") ?>

  <script>
    // Get the modal
    var modal_login = document.getElementById('btn_login');
    var modal_register = document.getElementById('btn_register');
    var wew = <?php echo $lat ?>;
    var wiw = <?php echo $len ?>;

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal_login) {
            modal_login.style.display = "none";
        }
    }
    window.onclick = function(event) {
        if (event.target == modal_register) {
            modal_register.style.display = "none";
        }
    }
    function myMap() {
      var myCenter = new google.maps.LatLng(wew,wiw);
      var mapCanvas = document.getElementById("googleMap");
      var mapOptions = {center: myCenter, zoom: 16};
      var map = new google.maps.Map(mapCanvas, mapOptions);
      var marker = new google.maps.Marker({position:myCenter});
      marker.setMap(map);
    // var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
        sendEvent = function(sel, step) {
        $(sel).trigger('next.m.' + step);
    }
    }
    </script>

    <?php @include("partial/script.php") ?>
</body>
</html>