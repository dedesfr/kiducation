// Trigger animation on scroll based on viewport
$(document).ready(function() {
  $('.vp-fadeinleft').viewportChecker({
      classToAdd: 'visible animated fadeInLeft',
      offset: 100
  });   
  $('.vp-fadeinright').viewportChecker({
      classToAdd: 'visible animated fadeInRight',
      offset: 100
  });  
  $('.vp-fadein').viewportChecker({
      classToAdd: 'visible animated fadeIn',
      offset: 100
  });
  $('.vp-fadeindown').viewportChecker({
      classToAdd: 'visible animated fadeInDown',
      offset: 100
  });
  $('.vp-fadeinup').viewportChecker({
      classToAdd: 'visible animated fadeInUp',
      offset: 100
  });
  $('.vp-slideinleft').viewportChecker({
    classToAdd: 'visible animated slideInLeft',
    offset: 100
  });  
  $('.vp-slideinright').viewportChecker({
    classToAdd: 'visible animated slideInRight',
    offset: 100
  });
  $('.vp-zoomin').viewportChecker({
    classToAdd: 'visible animated zoomIn',
    offset: 100
  });
  $('.vp-zoomindown').viewportChecker({
    classToAdd: 'visible animated zoomInDown',
    offset: 100
  });  
  $('.vp-rotatein').viewportChecker({
    classToAdd: 'visible animated rotateIn',
    offset: 100
  });  
  $('.vp-slideindown').viewportChecker({
    classToAdd: 'visible animated slideInDown',
    offset: 100
  });
  $('.parallax-window').parallax({parallax: 'scroll'});

}); 