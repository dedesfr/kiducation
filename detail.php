<?php
session_start();
include("base/koneksi.php");

$idUser = $_SESSION['idUser'];
$email = $_SESSION['email'];
if(isset($_SESSION['fullname'])){
    $fullname = $_SESSION['fullname'];
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php @include("partial/head.php") ?>
</head>
<body>
<?php @include("partial/navbar.php") ?>
  
  <div class="kiducation">
    <div class="detail-activities" style="position: relative;">
    <img alt="pattern" id="pattern" src="images/pattern2.png" style="position: absolute; top: 16em; left: 0;" width="200" />
    <img alt="pattern" id="pattern" src="images/pattern12.png" style="position: absolute; top: 16em; left: 80px;" width="100" />
    <img alt="pattern" id="pattern" src="images/pattern4.png" style="position: absolute; top: 60em; right: 30px;" width="150" />
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-1">
            <div class="img-wrapper">
              <img src="images/whatweoffer-2.jpg" class="img-responsive">
            </div>
            <div class="detail-title">Learn How To Make Recycled Pot & Flowers Frame</div>
            <div class="detail-caption-bold">“Once upon a time, there lived a lonely tree, Flora, who really wanted to have a lot of flower friends to accompany him during day and night.”</div>
            <div class="detail-caption-light">Hi Learners! Learners will learn to make flowers friend for Flora by making a recycled pot and flower frames. Not only that, they will also learn how to take care of plants and how important plants are to make our lives sustainable.</div>

            <div class="divider"></div>

            <div class="detail-information">
              <div class="row">
                <div class="col-sm-3">
                  <div class="detail-information-title">Start Date:</div>
                  <div class="detail-information-answer">Sunday, 25 Mar 2018<br>09.00 AM - 11.30 AM</div>
                </div>
                <div class="col-sm-3">
                  <div class="detail-information-title">Address:</div>
                  <div class="detail-information-answer">The Barn Restaurant, Educenter BSD Kav Commercial International School II No 8 BSD City, Jalan Sekolah Foresta, BSD City</div>
                </div>
                <div class="col-sm-3">
                  <div class="detail-information-title">Class Size:</div>
                  <div class="detail-information-answer">10</div>
                </div>
                <div class="col-sm-3">
                  <div class="detail-information-title">Minimum Age:</div>
                  <div class="detail-information-answer">3+</div>
                </div>
              </div>
              <div class="row" style="margin-top: 10px;">
                <div class="col-sm-3">
                  <div class="detail-information-title">Level</div>
                  <div class="detail-information-answer">Beginner</div>
                </div>
                <div class="col-sm-3">
                  <div class="detail-information-title">Language:</div>
                  <div class="detail-information-answer">Indonesia</div>
                </div>
                <div class="col-sm-3">
                  <div class="detail-information-title">Price:</div>
                  <div class="detail-information-answer">150000</div>
                </div>
                <div class="col-sm-3">
                  <div class="detail-information-title">Last date registration:</div>
                  <div class="detail-information-answer">Saturday, 24 Mar 2018, 5 PM</div>
                </div>
              </div>
            </div>

            <div class="divider"></div>

            <div class="detail-information-willyoulearn">
              <div class="detail-caption-bold">WHAT WILL YOU LEARN?</div>
              <div class="detail-caption-light">1. Participants will learn how to make a recycled pot and DIY flower frames.</div>
              <div class="detail-caption-light">2. As a part of this workshop, participants will also play games in order to bond as a team and to practice their cognitive and psychomotor skills.</div>
            </div>

            <div class="divider"></div>

            <div class="detail-information-willyoulearn">
              <div class="detail-caption-bold">WHAT IS THE PRICE INCLUSIVE OF?</div>
              <div class="detail-caption-light">1. Workshop materials</div>
              <div class="detail-caption-light">2. Lunch package from The Barn Restaurant</div>
              <div class="detail-caption-light">3. Certificate of participation</div>
            </div>

            <div class="divider"></div>

            <div class="detail-information-willyoulearn">
              <div class="detail-caption-bold">WHAT DO YOU NEED TO BRING?</div>
              <div class="detail-caption-light">1. Please wear comfortable clothes, shoes, and socks! Some of the activities will require participants to move a lot and play at the indoor playground.</div>
              <div class="detail-caption-light">2. As a preventive action, please bring a change of clothes in case learners' clothes are dirty from making the crafts.</div>
              <div class="detail-caption-light">3.Please bring your own water bottle.</div>
            </div>

            <div class="divider"></div>

            <div class="detail-information-willyoulearn">
              <div class="detail-caption-bold">FAQ</div>
              <div class="detail-caption-light"><strong>Q</strong>: What would be the children's lunch menu?</div>
              <div class="detail-caption-light"><strong>A</strong>: Lunch will be provided by The Barn Restaurant. At the re-registration, there would be some lunch choices and the participants are able to choose depending on their preference.</div>
              <br>
              <div class="detail-caption-light"><strong>Q</strong>: What if my children have an allergic / certain health conditions?</div>
              <div class="detail-caption-light"><strong>A</strong>: Please inform us if your children have an allergic or certain health condition that needs to be paid attention to.</div>
              <br>
              <div class="detail-caption-light"><strong>Q</strong>: Do guardians or parents need to wait for the participants?</div>
              <div class="detail-caption-light"><strong>A</strong>: To ensure participants' safety, once participants arrive at the venue of the event, every parents or guardian are obliged to fill in the Attendance List and take Guardian Card. After that, parents or guardian can wait at The Barn dining area or leave and pick up the participants once they are finished (Workshop will be done at 11.00 while lunch will start at 11.00 until 11.30).</div>
              <br>
              <div class="detail-caption-light"><strong>Q</strong>: What if I'm late to pick up my child?</div>
              <div class="detail-caption-light"><strong>A</strong>: Kiducation will ensure that each participant is picked up by their own parents or guardian according to the Attendance List. However, if the parents or guardians are late, we can assure that we would watch the participants and provide some activities to keep them busy until you come to pick them up.</div>
              <br>
              <div class="detail-caption-light"><strong>Q</strong>: Can I get my refund back if I am not able to make it last minute?</div>
              <div class="detail-caption-light"><strong>A</strong>: No.</div>
              <br>
              <div class="detail-caption-light"><strong>Q</strong>: Can I get my friend to take over if I am not able to make it last minute?</div>
              <div class="detail-caption-light"><strong>A</strong>: Yes. However, please kindly inform us for us to make any necessary arrangement.</div>
            </div>

            <div class="divider"></div>

            <div class="detail-information-willyoulearn">
              <div class="detail-caption-bold">TERMS & CONDITIONS</div>
              <div class="detail-caption-light">All payments are non-refundable and are not transferrable to other workshops but you are allowed to transfer your ticket to other person. Please kindly inform us so that we can make any necessary arrangement.</div>
              <br>
              <div class="detail-caption-light">In the event if the class is cancelled due to some unexpected events by the educator, we will refund your payment fully.</div>
              <br>
              <div class="detail-caption-light">In the event if the class information such as the price/time/location/learning description is amended by the educator/facilitator, you can have the option to ask for refund.</div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>

  <?php @include("partial/footer.php") ?>

  <script>
    // Get the modal
    var modal_login = document.getElementById('btn_login');
    var modal_register = document.getElementById('btn_register');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal_login) {
            modal_login.style.display = "none";
        }
    }
    window.onclick = function(event) {
        if (event.target == modal_register) {
            modal_register.style.display = "none";
        }
    }
    </script>
    <?php @include("partial/script.php") ?>
</body>
</html>