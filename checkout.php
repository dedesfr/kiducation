<?php
session_start();
include("base/koneksi.php");


if(isset($_SESSION['fullname'])){
  $fullname = $_SESSION['fullname'];
  $idUser = $_SESSION['idUser'];
  $email = $_SESSION['email'];
  $phone = $_SESSION['phone'];
}
if ($_SESSION['voucher_price_ses'] != null) {
  $voucher = $_SESSION['voucher_price_ses'];
  $voucher_id = $_SESSION['voucher_id_ses'];
} else {
  $voucher = 0;
}
if (isset($_POST['submit'])) {
  
    $nama = $_SESSION['nama'];
    $umur = $_SESSION['umur'];
    $notes = $_SESSION['notes'];
    $food = $_SESSION['food'];
    $workshop_id = $_SESSION['workshop_id'];
    $total = $_POST['total'];

    function autofalse(){ global $con; mysqli_autocommit($con, FALSE); }
    function commit(){ global $con; mysqli_commit($con); }
    function rollback(){ global $con; mysqli_rollback($con); }

    $flag = 0;
    $count = 0;

    try {
      autofalse();

      $userQ = mysqli_query($con, "SELECT * FROM users WHERE id ='$idUser'");
      $userResult = mysqli_fetch_array($userQ);

      # 1
      $bundleQ = mysqli_query($con, "SELECT * FROM bundle_discount");
      $bundleResult = mysqli_fetch_array($bundleQ);
      $bundlePrice = $bundleResult['price'];
      
      # 2
      if (count($nama) > 1) {
        $x = count($nama);
        $y = $x/2;

        if ($x%2 == 0) {
          $bundlePrice = $bundlePrice * $y;
          $total = $total - $bundlePrice;          
        } else {
          $bundlePrice = $bundlePrice * ($x-1)/2;
          $total = $total - $bundlePrice;          
        }
      }
      if ($voucher != 0) {
        $total2 = $total - $voucher;
        $total2 = $total2 + rand(1,50);
        $orderQuery = mysqli_query($con, "INSERT INTO tr_order(user_id, workshop_id, status, voucher_id, isUserFirstDiscount, total) VALUES ($idUser, $workshop_id, 'pending', $voucher_id, 'no', $total2)");
        
      } else {
        $total2 = $total;
        $total2 = $total2 + rand(1,50);
        $orderQuery = mysqli_query($con, "INSERT INTO tr_order(user_id, workshop_id, status, isUserFirstDiscount, total) VALUES ($idUser, $workshop_id, 'pending', 'no', $total2)");
      }
  
      if (!$orderQuery) {
          $flag = false;
          throw new Exception("Something went wrong, please contact our Customer Support. code: win1");
      } else {
          $last_order_id = $con->insert_id;

          $updateNoOrder = mysqli_query($con, "UPDATE tr_order SET no_order = 'WS-$last_order_id-$workshop_id' WHERE id = '$last_order_id'");

          if (!$updateNoOrder) {
            $flag = 1;
            throw new Exception("Something went wrong, please contact our Customer Support. code: win4");
          } else {
            $updateUser = mysqli_query($con, "UPDATE users SET firstDiscount = 'no' WHERE id = '$idUser'");

            if (!$updateUser) {
              $flag = 1;
              throw new Exception("Something went wrong, please contact our Customer Support. code: win5");
            } else {

              foreach ($nama as $key => $value) {
              
                $participantInsert = mysqli_query($con, "INSERT INTO participants(user_id, order_id, food_id, nama, umur, notes) VALUES ($idUser, $last_order_id, '". $con->real_escape_string($food[$key]) ."', '". $con->real_escape_string($value) ."', '". $con->real_escape_string($umur[$key]) ."', '". $con->real_escape_string($notes[$key]) ."')");
        
                if (!$participantInsert) {
                    $flag = 1;
                    throw new Exception("Something went wrong, please contact our Customer Support. code: win2");
                } else {
                    $count += 1;
                    $foodQuery = mysqli_query($con, "INSERT INTO tr_food(food_id, order_id) VALUES('". $con->real_escape_string($food[$key]) ."', $last_order_id)");
        
                    $foodResult = $con->query($foodQuery);
            
                    if (!$foodQuery) {
                        $flag = 1;
                        throw new Exception("Something went wrong, please contact our Customer Support. code: win3");
                    } else {
                      $workshopQuery = mysqli_query($con, "select * FROM tr_workshop WHERE w_id = $workshop_id");
                      $workshopResult = mysqli_fetch_array($workshopQuery);
                  
                      if (!$workshopResult) {
                          $flag = 1;
                          echo "Error details: " . mysqli_error($con) . ".";
                      } 
                    }
                }
            }
            }
          }
      }

      if ($flag == 0) {
        commit();
      } else {
        rollback();
        header("location:index.php?iType=2&&infoatas=".$e->getMessage());
      }
    } catch(Exception $e){
      rollback();
      header("location:../index.php?iType=2&&infobawah=".$e->getMessage());
    }

  } else {
    header("location:activities.php");
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="images/kidu-square.png" type="image/png" />
    <title>kiducation</title>
    <link rel="stylesheet" href="dist/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="stylesheets/index.css">
    <link href="https://fonts.googleapis.com/css?family=Anton|EB+Garamond|Josefin+Slab|Lobster" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

</head>
<body>
  <?php @include("partial/navbar.php") ?>

  <!-- Workshop list -->
  <div class="kiducation" style="position: relative;">
    <img alt="Logo Kiducation" id="pattern" src="images/pattern3.png" style="position: absolute; top: 90px; left: 26em;" width="150" />
    <img alt="Logo Kiducation" id="pattern" src="images/pattern17.png" style="position: absolute; top: 150px; right:25em;" width="150" />
    <img alt="Logo Kiducation" id="pattern" src="images/pattern4.png" style="position: absolute; top: 30em; right: 26px;" width="150" />
    <img alt="Logo Kiducation" id="pattern" src="images/pattern16.png" style="position: absolute; top: 30em; left: 50px;" width="250" />
    <div class="login" style=" position: relative; padding-top: 50px; padding-bottom: 50px;">
      <h1 style="text-align: center; color: #4988cd; ">Payment Checkout</h1>
      <div class="container container-xs">
        <div class="card shadow" style="padding: 20px;">
        <!-- <img alt="Logo Kiducation" id="pattern" src="images/pattern2.png" style="position: absolute; right: -52px; top: -116px; transform: rotate(90deg);" width="200" />
        <img alt="Logo Kiducation" id="pattern" src="images/pattern2.png" style="position: absolute; left: -126px; bottom: -113px; transform: rotate(90deg);" width="200" /> -->
          <div class="card-body">
            <div class="alert alert-danger text-center">Please make sure you are inputting the right bank account number!</div>
            <p>Please make payment by transferring to the following bank account :</p>
            <p class="text-center" style="font-size: 20px; font-weight: bold;"><img src="gambar/bca-logo.svg" style="width: 100px; margin-right: 10px"> 6041436988</p>
            <p class="text-center">a/n Rainer Hartanto</p>
            <hr>
            <span>No Order: <span style="font-weight: bold; font-size: 18px;"><?php echo "WS-".$last_order_id."-".$workshop_id; ?></span></span><br>
            <?php if (count($nama) > 1) { ?>
              <span>Total : <span style="font-weight: bold; font-size: 18px;">Rp. <?php echo $total2 + $voucher + $bundlePrice; ?></span></span><br>
            <?php } else { ?>
              <span>Total : <span style="font-weight: bold; font-size: 18px;">Rp. <?php echo $total2 + $voucher; ?></span></span><br>
            <?php } ?>
            <?php if (count($nama) > 1) { ?>
              <span>Bundle Discount : <span style="font-weight: bold; font-size: 18px;">Rp. <?php echo $bundlePrice; ?></span></span><br>
            <?php } ?>
            <?php if (isset($_SESSION['voucher_price_ses'])) { ?>
              <span>Voucher Discount : <span style="font-weight: bold; font-size: 18px;">Rp. <?php echo $voucher; ?></span></span>
              <br>
            <?php } ?>
              <span>Total After Discount : <span style="font-weight: bold; font-size: 18px;">Rp. <?php echo $total2; ?></span></span><br>
            <br>
            <div class="alert alert-info" style="margin-top: 2rem;">All payment must be made within <strong>24 hours</strong> or your order will be cancelled automatically.</div> 
            <a href="checkpayment.php" class="btn btn-info btn-block">Check Your Transaction Status</a>
          </div>
        </div><!-- /card-container -->
      </div><!-- /container -->
    </div>
  </div>
  <!-- end Workshop list -->

  <?php @include("partial/footer.php") ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAtCa03_4E99XlF75_Nr512E_W1tQfmqwE&callback=myMap"></script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="dist/bootstrap/js/bootstrap.min.js"></script>
    <script src="dist/multi-step-modal/multi-step-modal.js"></script>
    <script src="dist/sweetalert/sweetalert.min.js"></script>

</body>
</html>