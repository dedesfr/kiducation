<?php
session_start();
include("../../../base/koneksi.php");

if (isset($_POST['submit'])) {
  
    $nama = $_POST['nama'];
    $umur = $_POST['umur'];
    $notes = $_POST['notes'];
    $food = $_POST['food'];
    $id_user = $_SESSION['idUser'];
    $fullname = $_SESSION['fullname'];
    $email = $_SESSION['email'];
    $phone = $_SESSION['phone'];
    $workshop_id = $_POST['workshop_id'];

    mysqli_autocommit($con, false);

    $flag = true;
    $count = 0;

    $con->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);

    $orderQuery = "INSERT INTO tr_order(user_id, workshop_id, status) VALUES ($id_user, $workshop_id, 'pending')";  
    
    $orderResult = $con->query($orderQuery);

    if (!$orderResult) {
        $flag = false;
        echo "Error details: " . mysqli_error($con) . ".";
    } else {
        $last_order_id = $con->insert_id;
    }

    foreach ($nama as $key => $value) {
        
        $participantInsert = "INSERT INTO participants(user_id, order_id, food_id, nama, umur, notes) VALUES ($id_user, $last_order_id, '". $con->real_escape_string($food[$key]) ."', '". $con->real_escape_string($value) ."', '". $con->real_escape_string($umur[$key]) ."', '". $con->real_escape_string($notes[$key]) ."')";
    
        $participantResult = $con->query($participantInsert);

        if (!$participantResult) {
            $flag = false;
            echo "Error details: " . mysqli_error($con) . ".";
        } else {
            $count += 1;
        }

        $foodQuery = "INSERT INTO tr_food(food_id, order_id) VALUES('". $con->real_escape_string($food[$key]) ."', $last_order_id)";

        $foodResult = $con->query($foodQuery);

        if (!$foodResult) {
            $flag = false;
            echo "Error details: " . mysqli_error($con) . ".";
        }
    }

    $workshopQuery = mysqli_query($con, "select * FROM tr_workshop WHERE w_id = $workshop_id");
    $workshopResult = mysqli_fetch_array($workshopQuery);

    if (!$workshopResult) {
        $flag = false;
        echo "Error details: " . mysqli_error($con) . ".";
    }

    if ($flag) {
        mysqli_commit($con);
    } else {
        mysqli_rollback($con);
    }
  }

require_once(dirname(__FILE__) . '/../../Veritrans.php');
//Set Your server key
Veritrans_Config::$serverKey = "SB-Mid-server-eU1eGOwWhwMcjXLhDs4RQ6Gm";

// Uncomment for production environment
// Veritrans_Config::$isProduction = true;

// Uncomment to enable sanitization
// Veritrans_Config::$isSanitized = true;

// Uncomment to enable 3D-Secure
// Veritrans_Config::$is3ds = true;

// Required
$transaction_details = array(
  'order_id' => $last_order_id,
  'gross_amount' => $workshopResult['w_price'] * $count, // no decimal allowed for creditcard
  );

// Optional
$item1_details = array(
    'id' => 'a1',
    'price' => $workshopResult['w_price'],
    'quantity' => $count,
    'name' => "Participant"
    );

// Optional
// $item2_details = array(
//     'id' => 'a2',
//     'price' => 45000,
//     'quantity' => 1,
//     'name' => "Orange"
//     );

// Optional
$item_details = array ($item1_details, $item2_details);

// Optional
$billing_address = array(
    'first_name'    => $_SESSION['fullname'],
    'last_name'     => "Litani",
    'address'       => "Mangga 20",
    'city'          => "Jakarta",
    'postal_code'   => "16602",
    'phone'         => "081122334455",
    'country_code'  => 'IDN'
    );

// Optional
$shipping_address = array(
    'first_name'    => "Obet",
    'last_name'     => "Supriadi",
    'address'       => "Manggis 90",
    'city'          => "Jakarta",
    'postal_code'   => "16601",
    'phone'         => "08113366345",
    'country_code'  => 'IDN'
    );

// Optional
$customer_details = array(
    'first_name'    => $fullname,
    'email'         => $email,
    'phone'         => $phone,
    'billing_address'  => $billing_address,
    'shipping_address' => $shipping_address
    );

// Fill transaction details
$transaction = array(
    'transaction_details' => $transaction_details,
    'customer_details' => $customer_details,
    'item_details' => $item_details,
    );

try {
  // Redirect to Veritrans VTWeb page
  header('Location: ' . Veritrans_VtWeb::getRedirectionUrl($transaction));
}
catch (Exception $e) {
  echo $e->getMessage();
  if(strpos ($e->getMessage(), "Access denied due to unauthorized")){
      echo "<code>";
      echo "<h4>Please set real server key from sandbox</h4>";
      echo "In file: " . __FILE__;
      echo "<br>";
      echo "<br>";
      echo htmlspecialchars('Veritrans_Config::$serverKey = \'SB-Mid-server-eU1eGOwWhwMcjXLhDs4RQ6Gm\';');
      die();
}

}
