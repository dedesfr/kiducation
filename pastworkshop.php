<?php
session_start();
include("base/koneksi.php");

$id = "";
$id = @$_GET['id'];

if(isset($_SESSION['fullname'])){
  $fullname = $_SESSION['fullname'];
  $idUser = $_SESSION['idUser'];
  $email = $_SESSION['email'];
  $phone = $_SESSION['phone'];
}

if (isset($_POST['submit'])) {
  
  $nama = $_POST['nama'];
  $umur = $_POST['umur'];

  foreach ($nama as $key => $value) {
    
  }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php @include("partial/head.php") ?>
</head>
<body>
<?php @include("partial/navbar.php") ?>

  <!-- Workshop list -->
  <?php if($id == ""){ ?>
    <div class="kiducation">
      <div class="container">
        <div class="pastworkshop" >
          <div class="row">
            <div class="col-sm-12" style="margin-bottom: 40px; position: relative;">
              <img alt="pattern" class="pattern-move" id="pattern" src="images/pattern18.png" style="position: absolute; top: 0;" width="100" />
              <img alt="pattern" class="pattern-hover" id="pattern" src="images/pattern10.png" style="position: absolute; bottom: 0; right: 0;" width="50" />
              <h1 class="upcoming-title">past workshop</h1>
            </div>
            <!-- activity looping -->
              <?php
                $result = mysqli_query($con, "SELECT COUNT(*) FROM tr_workshop");
                $r = mysqli_fetch_row($result);
                $numrows = $r[0];

                $result = mysqli_query($con, "SELECT * FROM tr_workshop WHERE isShowPastWorkshop = 'yes' ORDER BY w_date DESC");
                while ($query = mysqli_fetch_assoc($result)) {
                $datee = new dateTime($query['w_date']);
                $datee = $datee->format('l, F j Y');
              ?>
                <div class="col-sm-8 col-sm-offset-2">
                  <div class="past-workshop-wrapper row">
                    <div class="col-sm-6">
                      <a href="detail-pastworkshop.php?id=<?php echo $query['w_id']; ?>" class="a-none">          
                      <img src="<?php echo $query['w_picture']; ?>" alt="<?php echo $query['w_header']; ?>" class="img-responsive">
                    </div>
                    <div class="col-sm-6">
                      <div class="img-title" style="margin: 0;"><?php echo $query['w_header']; ?></div>
                      </a>
                      <div class="img-caption text-truncate-multiline mb-1"><?php echo $query['w_subheader']; ?></div>
                      <div class="img-caption">Date : <?php echo $datee; ?></div>
                      <div class="img-caption text-truncate-twoline">Location : <?php echo $query['w_address']; ?> </div>
                      <a href="pastworkshop.php?id=<?php echo $query['w_id']; ?>" class="pull-right a-none">See Gallery</a>
                    </div>
                  </div>
                </div>              
              <?php } ?>
            <!-- activity looping -->
          </div>
        </div>
      </div>
    </div>
  <?php } ?>
  <!-- end Workshop list -->

  <!-- past workshop detail -->
  <?php if($id != ""){ 
    $workshopQ = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tr_workshop WHERE w_id = $id"));
    ?>
    <div class="kiducation">
      <section class="py-main detailpastworkshop">
        <div class="container">
          <h3 class="text-center heading"><?php echo $workshopQ['w_header'] ?></h3>
          <div class="text-center"><?php echo $workshopQ['w_content'] ?></div>
          <div class="row mt-5">
            <!-- DE, NANTI LOOPING DISINI YAAAA -->
            <!-- <div class="col-md-4 col-sm-6">
              <div class="img-wrapper" style="background: linear-gradient(rgba(0,0,0,.3),rgba(0,0,0,.3)), url(<?php echo $query['w_picture']; ?>) no-repeat center; background-size: cover; height: 300px;">
              </div>
            </div> -->
            <!-- DE, NANTI LOOPING DISINI YAAAA -->

            <?php
              $workshop_galleryQ = mysqli_query($con, "SELECT * FROM past_workshop WHERE workshop_id = $id");
              while($workshop_gallery = mysqli_fetch_array($workshop_galleryQ)) {
            ?>
              <div class="col-sm-4 mb-3">
                <a href="<?php echo $workshop_gallery['images'] ?>" data-fancybox="gallery">
                  <div style="background: url(<?php echo $workshop_gallery['images'] ?>) no-repeat center; background-size: cover; height: 350px;"></div>
                </a>
              </div>
            <?php } ?>
            
            <!-- youtube -->
            <div class="col-sm-8 col-sm-offset-2" style="margin-top: 3rem;">
              <a href= <?php echo $workshopQ['w_youtubeUrl'] ?> data-fancybox="gallery">
                <iframe  class="youtube-frame" style="margin: 0 auto;" src= <?php echo $workshopQ['w_youtubeUrl'] ?>></iframe>
              </a>
            </div>
            <!-- youtube -->

          </div>
        </div>
      </section>
    </div>
  <?php } ?>
  <!-- end past workshop detail -->

  <?php @include("partial/footer.php") ?>
  <?php @include("partial/script.php") ?>
</body>
</html>