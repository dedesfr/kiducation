<?php
session_start();
include("base/koneksi.php");


if(isset($_SESSION['fullname'])){
  $fullname = $_SESSION['fullname'];
  $idUser = $_SESSION['idUser'];
  $email = $_SESSION['email'];
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <?php @include("partial/head.php") ?>
</head>

<body>
  <?php @include("partial/navbar.php") ?>
  <div class="kiducation">

    <!-- <div class="navbar">
      <ul class="nav navbar-nav">
        <li>
          <a href="aboutus.php">about</a>
        </li>
        <li>
          <a href="#">activities</a>
        </li>
        <li>
          <a href="#">blog</a>
        </li>
        <li>
          <a href="index.php">
            <img alt="Logo Kiducation" id="logo-navbar-middle" src="images/logokidu.png" width="200" />
          </a>
        </li>
        <li>
          <a href="#">get in touch</a>
        </li>
        <li>
          <a href="#">my account</a>
        </li>
        <li>
          <button onclick="document.getElementById('btn_login').style.display='block'" style="width:auto;">Login</button>
        </li>
        <li>
          <button onclick="document.getElementById('btn_register').style.display='block'" style="width:auto;">Register</button>
        </li>
      </ul>
    </div> -->

    <div class="hero">
      <div class="container">
        <div class="row">
          <div class="col-sm-6" style="position: relative;">
            <img alt="Logo Kiducation" id="pattern" src="images/pattern6.png" style="position: absolute; top: -43px; transform: rotate(-19deg);" width="200" />
            <h1 class="hero-title animated fadeInUp delayp1">Hello</h1>
            <h4 class="hero-subtitle animated fadeInLeft delayp2">
              Kiducation is a children workshop organizer based in BSD City, Tangerang, Banten. We specialize in arts and crafts activities as it is proved to be beneficial for children development. We, however, believe that learning should not be boring. Our workshops are supplemented with educational games, all wrapped up in exciting themes and storylines!
            </h4>
          </div>
          <div class="col-sm-6 hidden-xs">
            <div class="pattern-1" style="position: relative;">
              <img alt="Logo Kiducation" id="pattern" src="images/pattern2.png" style="position: absolute; right: -17px; top: 16em;" width="150" />
              <img alt="Logo Kiducation" class="pattern-hover" id="pattern" src="images/pattern1.png" style="position: absolute; right: -48px; top: 16em;" width="150" />

              <!-- <h2 class="heading-title">Upcoming Workshop</h2>   
              <div class="fadingEffect" style="background-color: #4988cd;"></div> -->

            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- section2 -->
    <div class="hero-divider">
      <div class="container">
        <div class="row">
          <div class="col-sm-12" style="padding: 20px;">
            <div class="typewriter">
              <?php
                $headlineQ = mysqli_query($con, "select id, title FROM headline ORDER BY id DESC");
                while($headline = mysqli_fetch_array($headlineQ)){    
              ?>
              <h4><?php echo $headline['title']; ?></h4>
              <?php } ?>
            </div>
            <!-- <span style="color: #ffb5a8;"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. </span> -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- section2 -->

  <!-- <section class="py-main">
    <div class="container">
      <h3 class="animated fadeInUp delayp1 text-center">Upcoming Workshop</h3>

    </div>
  </section> -->

  <!-- section3 -->
  <div class="whatweoffer bg-grey-light">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 animated fadeInUp delayp1">
          <div class="whatweoffer-title">what we offer</div>
        </div>
        <div class="col-sm-6" style="padding: 50px; position: relative;">
          <img alt="pattern" id="pattern" src="images/pattern5.png" style="position: absolute; left: 13px; bottom: 16em;" width="80" />
          <div class="img-wrapper">
            <img src="images/whatweoffer-1.png" class="img-responsive">
          </div>
          <div class="img-title animated fadeInUp delayp2">Fun arts and crafts workshops</div>
          <div class="img-caption animated fadeInUp delayp2" style="min-height: 90px;">Fun arts and crafts workshop for your kids held on Sunday morning at The Barn Restaurant, BSD, Tangerang.</div>
          <button class="btn-kiducation btn-block btn-lg" style="margin-top: 12px;">
            <a href="activities.php">See More</a>
          </button>
        </div>
        <div class="col-sm-6" style="padding: 50px; position: relative;">
          <img alt="pattern" id="pattern" src="images/pattern3.png" style="position: absolute; right: 0; top: 0;" width="200"/>
          <img alt="pattern" id="pattern" src="images/pattern4.png" class="pattern-hover" style="position: absolute; left: 3px; bottom: 13em;" width="50" />
          <div class="img-wrapper">
            <img src="images/whatweoffer-2.png" class="img-responsive">
          </div>
          <div class="img-title animated fadeInUp delayp3">Workshop partnership</div>
          <div class="img-caption animated fadeInUp delayp3" style="min-height: 90px;">Ready to partner with you to organize exciting arts and crafts activities, tailored to your preferences and needs. Available for birthday parties, school activities, community events, and playdates.</div>
          <button class="btn-kiducation btn-block btn-lg" style="margin-top: 12px;">
            <a href="organize.php">See More</a>
          </button>
        </div>
      </div>
    </div>
  </div>
  <!-- section3 -->

  <!-- section4 -->
  <!-- <div class="testimonial">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="testimonial-title">testimonial</div>
          <div class="testimonial-content">
            <div class="col-sm-4">
              <img alt="Logo Kiducation" id="pattern" src="images/pattern8.png" style="position: absolute; bottom: -46px; left: -20px; z-index: 100;" width="100" />
              <div class="testi-box">
                <img src="images/testi-1.jpg" class="img-circle img-testi img-responsive">
                <div class="testi-testi">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo, a ratione autem vero, asperiores reiciendis iure, minus magni, sunt quia provident dolorem rem hic. Ullam consectetur reiciendis veniam, ea possimus!</div>
                <div class="testi-author">Ivan Gunawan</div>
              </div>
            </div>

            <div class="col-sm-4">
              <div class="testi-box">
                <img src="images/testi-2.jpg" class="img-circle img-testi img-responsive">
                <div class="testi-testi">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo, a ratione autem vero, asperiores reiciendis iure, minus magni, sunt quia provident dolorem rem hic. Ullam consectetur reiciendis veniam, ea possimus!</div>
                <div class="testi-author">eli sugigi</div>
              </div>
            </div>

            <div class="col-sm-4">
              <img alt="pattern" id="pattern" src="images/pattern7.png" style="position: absolute; top: -45px; right: 0; z-index: 100;" width="100" />
              <div class="testi-box">
                <img src="images/testi-3.jpeg" class="img-circle img-testi img-responsive">
                <div class="testi-testi">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo, a ratione autem vero, asperiores reiciendis iure, minus magni, sunt quia provident dolorem rem hic. Ullam consectetur reiciendis veniam, ea possimus!</div>
                <div class="testi-author">anisa bahar</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> -->
  <!-- section4 -->

  <?php @include("partial/footer.php") ?>

  <script>
    // Get the modal
    var modal_login = document.getElementById('btn_login');
    var modal_register = document.getElementById('btn_register');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal_login) {
            modal_login.style.display = "none";
        }
    }
    window.onclick = function(event) {
        if (event.target == modal_register) {
            modal_register.style.display = "none";
        }
    }
    </script>
    
    <script>
      $(document).ready(function() {
  $('.vp-fadeinleft').viewportChecker({
      classToAdd: 'visible animated fadeInLeft',
      offset: 100
  });   
  $('.vp-fadeinright').viewportChecker({
      classToAdd: 'visible animated fadeInRight',
      offset: 100
  });  
  $('.vp-fadein').viewportChecker({
      classToAdd: 'visible animated fadeIn',
      offset: 100
  });
  $('.vp-fadeindown').viewportChecker({
      classToAdd: 'visible animated fadeInDown',
      offset: 100
  });
  $('.vp-fadeinup').viewportChecker({
      classToAdd: 'visible animated fadeInUp',
      offset: 100
  });
  $('.vp-slideinleft').viewportChecker({
    classToAdd: 'visible animated slideInLeft',
    offset: 100
  });  
  $('.vp-slideinright').viewportChecker({
    classToAdd: 'visible animated slideInRight',
    offset: 100
  });
  $('.vp-zoomin').viewportChecker({
    classToAdd: 'visible animated zoomIn',
    offset: 100
  });
  $('.vp-zoomindown').viewportChecker({
    classToAdd: 'visible animated zoomInDown',
    offset: 100
  });  
  $('.vp-rotatein').viewportChecker({
    classToAdd: 'visible animated rotateIn',
    offset: 100
  });  
  $('.vp-slideindown').viewportChecker({
    classToAdd: 'visible animated slideInDown',
    offset: 100
  });
  $('.parallax-window').parallax({parallax: 'scroll'});

}); 
    </script>

    <?php @include("partial/script.php") ?>
</body>
</html>