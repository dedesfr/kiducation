<?php
session_start();
include("base/koneksi.php");
$page 		= "account";
$pagetree	= "account";

$idadmin = $_SESSION['idadmin'];
if($idadmin == ""){
	$_SESSION['error'] = "Silahkan login terlebih dahulu";
	header("location:index.php");
}

$namaOpr = $_SESSION['nama'];

$info = "";
$info = @$_GET['info'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>DAB Administrator</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
	<link rel="stylesheet" href="dist/css/ionicons-2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  
  <?php include "base/header.php"; ?>
  <?php include "base/sidebar.html"; ?>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        My Account Information<small><?php echo $info; ?></small></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
		
		<!-- Modifikasi Data Operator -->
		<?php 
		$editOprQ = mysqli_query($con, "select * FROM ms_operator WHERE opr_id = $idadmin");
		$editOpr = mysqli_fetch_array($editOprQ);
		?>
		<form action="scripts/account.php" method="post">
		<input type="hidden" value="<?php echo $idadmin; ?>" name="idOperator" />
		<div class="col-md-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Modifikasi Informasi Data Akun</h3>
            </div>
			
              <div class="box-body">
			  
				<!-- line 1 -->
                <div class="form-group col-md-4">
                  <label for="opr_nama" class="col-sm-12 control-label">Nama User</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="opr_nama" value="<?php echo $editOpr['opr_nama']; ?>">
                  </div>
                </div>
				
                <div class="form-group col-md-4">
                  <label for="opr_username" class="col-sm-12 control-label">Username</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="opr_username" value="<?php echo $editOpr['opr_username']; ?>">
                  </div>
                </div>
				
				<div class="form-group col-md-4">
                  <label for="opr_password" class="col-sm-12 control-label">Password</label>
                  <div class="col-sm-12">
                    <input type="password" class="form-control" name="opr_password" placeholder="isi password baru disini">
                  </div>
                </div>

              </div>
              
			  <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <a href="account.php" class="btn btn-default">Reset</a>
			  </div>
			  
          </div>
        </div>
        </form>
		<!-- /Modifikasi Data Operator -->
        
      </div>
    </section>
  </div>
  <?php include "base/footer.html"; ?>
</div>

<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/demo.js"></script>
</body>
</html>