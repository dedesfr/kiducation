<?php
session_start();
include("base/koneksi.php");
$page		= "order";
$pagetree	= "order";
date_default_timezone_set("Asia/Jakarta");

$idadmin = $_SESSION['idadmin'];
if($idadmin == ""){
	$_SESSION['error'] = "Silahkan login terlebih dahulu";
	header("location:index.php");
}
$namaOpr = $_SESSION['nama'];

$info = "";
$info = @$_GET['info'];

$id = "";
$id = @$_GET['id'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Kiducation Administrator</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="icon" href="base/favicon.png" type="image/x-icon">

	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
	<link rel="stylesheet" href="dist/css/ionicons-2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">
	<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
	<link rel="stylesheet" href="plugins/datetimepicker/css/bootstrap-datetimepicker.min.css">
	<script src="ckeditor/ckeditor.js"></script>
	<script src="ckeditor/config.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  
  <?php include "base/header.php"; ?>
  <?php include "base/sidebar.html"; ?>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>Order Editor<small><?php echo $info; ?></small></h1>
    </section>

    <section class="content">
      <div class="row">
	  
		<!-- Location -->
		<div class="col-md-12">
		  <div class="box">
            <div class="box-header">
              <h3 class="box-title">Order</h3>
            </div>
            <div class="box-body">
            <div id="load-order">
                <table id="example" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Order Number</th>
                            <th>Guardian Name</th>
                            <th>Guardian Email</th>
                            <th>Attachment</th>
                            <th>Action</th>
                            <th>Detail</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $orderQ = mysqli_query($con, "SELECT * FROM tr_order WHERE status = 'pending' ORDER BY id DESC");
                        while($order = mysqli_fetch_array($orderQ)){
                            $workshop_id = $order['workshop_id'];
                            $user_id = $order['user_id'];
                            $workshopQ = mysqli_query($con, "SELECT * FROM tr_workshop WHERE w_id = $workshop_id");
                            $workshop = mysqli_fetch_array($workshopQ);
                            $userQ = mysqli_query($con, "SELECT * FROM users WHERE id = $user_id");
                            $user = mysqli_fetch_array($userQ);
                        ?>
                        <tr>
                            <td><?php echo $order['no_order']; ?></td>
                            <td><?php echo $user['fullname']; ?></td>
                            <td><?php echo $user['email']; ?></td>
                            <td><img src= <?php echo "../".$order['attachment']?> alt="ga ada" width="300px" height="200px"></td>
                            <!-- <td><a data-id="<?php echo $order['id']; ?>" class="btn btn-primary approve_order">Approve</a> </td> -->
                            <td><a href="orderApprove.php?order_id=<?php echo $order['id']; ?>" class="btn btn-primary approve_order">Approve</a> </td>
                            <td><a href="order.php?id=<?php echo $order['id']; ?>">See Details</a></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            </div>
        </div>
        </div>
        <?php if($id != ""){ ?>
            <div class="container">
                <table class="table">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Participant Data</th>
                      <th>Registration Fee</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php $result = mysqli_query($con, "SELECT * FROM participants WHERE order_id = '$id'");
                    $x = mysqli_num_rows($result);
                    $y = $x/2;
                    $count = 0;
                    while ($query = mysqli_fetch_assoc($result)) {
                        $count++;
                        $foodid = $query['food_id'];
                        $orderQ = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tr_order WHERE id = '$id'"));
                        $bundleQ = mysqli_query($con, "SELECT * FROM bundle_discount");
                        $bundleResult = mysqli_fetch_array($bundleQ);
                        $bundlePrice = $bundleResult['price'];
                        # bundle query disini
                        $workshopId = $orderQ['workshop_id'];
                        $voucherId = $orderQ['voucher_id'];
                    ?>
                    <tr>
                      <td><?php echo $count; ?></td>
                      <td>
                        <p>Participant <?php echo $count;?></p>
                        <p>Name : <?php echo $query['nama'];?></p>
                        <p>Age: <?php echo $query['umur'];?></p>
                        <p>Lunch Menu:</p>
                        <p><?php $foodQ = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM food WHERE id = '$foodid'")); echo $foodQ['nama']?></p>
                        <p>Food Notes:</p>
                        <p><?php echo $query['notes']?></p>
                      </td>
                      <td><?php $workspacePriceQ = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tr_workshop WHERE w_id = '$workshopId'")); echo $workspacePriceQ['w_price']?></td>
                    </tr>
                  <?php }?>
                  </tbody>
                  <tbody>
                    <tr>
                        <td></td>
                        <td>Bundle</td>
                        <?php if ($x > 1) {
                            if ($x%2 == 0) { 
                                $bundlePrice = $bundlePrice * $y; ?>
                            <td><?php echo $bundlePrice; ?></td>
                            <?php } else { 
                                $bundlePrice = $bundlePrice * ($x-1)/2; ?>
                                <td><?php echo $bundlePrice ?></td>
                            <?php } ?>
                        <?php } else{ ?>
                            <td>no</td>
                        <?php } ?>
                    </tr>
                    <!-- bundle discount tr disini -->
                    <tr>
                        <td></td>
                        <td>Voucher</td>
                        <?php if ($voucherId == null) { ?>
                            <td>no</td>
                        <?php } else{ ?>
                            <td><?php $voucherQ = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM voucher WHERE id = '$voucherId'")); echo $voucherQ['name']?> <span>(<?php echo $voucherQ['price'] ?>)</span></td>
                        <?php } ?>
                    </tr>
                    <tr>
                      <td></td>
                      <td>Total</td>
                      <td><?php echo $orderQ['total']?></td>
                    </tr>
                  </tbody>
                  <!-- <p>total : </p> -->
                </table>
            </div>
        <?php } ?>
		<!-- /Location -->
      </div>
    </section>
  </div>
  <?php include "base/footer.html"; ?>
</div>

<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/demo.js"></script>
<script src="../dist/sweetalert/sweetalert.min.js"></script>

<script>
    $('#example').DataTable();
    // $('.approve_order').click(function (e) {
    //     var order_id = $(this).data('id');
    //     var dataPass = 'order_id=' + order_id
    //     var $ele = $(this).parent().parent();

    //     swal({
    //         title: "Are you sure?",
    //         text: "Order Approved Confirmation",
    //         icon: "info",
    //         buttons: true,
    //         })
    //         .then((willDelete) => {
    //         if (willDelete) {
    //             $.ajax({
    //                 type: "POST",
    //                 url: "orderApprove.php",
    //                 data: dataPass,
    //                 cache: false,
    //                 success: function(result){
    //                     swal("Poof! Order approved successfully.", {
    //                     icon: "success",
    //                     });
    //                     $ele.fadeOut(2000);
    //                 }
    //             });
    //         } else {
    //             swal("Cancelled");
    //         }
    //     });
        
    // });

</script>
</body>
</html>