<?php
session_start();
include("base/koneksi.php");
$page 		= "home";
$pagetree	= "home";

$idadmin = $_SESSION['idadmin'];

if($idadmin == ""){
	$_SESSION['error'] = "Silahkan login terlebih dahulu";
	header("location:index.php");
}
$namaOpr = $_SESSION['nama'];

$id = "";
$id = @$_GET['id'];

$info = "";
$info = @$_GET['info'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>DAB Administrator</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="icon" href="base/favicon.png" type="image/x-icon">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
	<link rel="stylesheet" href="dist/css/ionicons-2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">

	<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	<script src="ckeditor/ckeditor.js"></script>
	<script src="ckeditor/config.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include "base/header.php"; ?>
  <?php include "base/sidebar.html"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Homepage Slider<small><?php echo $info; ?></small></h1>
    </section>

    <section class="content">
      <div class="row">
	  
		<!-- slider -->
		<div class="col-md-12">
		  <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Homepage Slider <small>(refresh halaman ini jika Image belum berubah)</small></h3>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-hover">
                <thead>
                <tr>
                  <th>Action</th>
				  <th>Text 1</th>
				  <th>Text 2</th>
				  <th>Slider</th>
                </tr>
                </thead>
                <tbody>
                <?php
				$homepageQ = mysqli_query($con, "select * FROM ms_slider ORDER BY sli_id ASC");
				while($homepage = mysqli_fetch_array($homepageQ)){
				?>
				<tr>
				  <td><a href="dashboard.php?id=<?php echo $homepage['sli_id']; ?>">Edit</a> |  <a href="scripts/slider-delete.php?id=<?php echo $homepage['sli_id']; ?>">Delete</a></td>
                  <td><?php echo $homepage['sli_text1']; ?></td>
                  <td><?php echo $homepage['sli_text2']; ?></td>
                  <td><img src="../<?php echo $homepage['sli_image']; ?>" height="100px" /></td>
                </tr>
                <?php } ?>
				</tbody>
              </table>
            </div>
          </div>
        </div>
		<!-- /slider -->
        
        <!-- tambah slider -->
		<?php if($id == ""){ ?>
		<form enctype="multipart/form-data" action="scripts/slider.php" method="post">
		<input type="hidden" value="x" name="id" />
		<div class="col-md-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Slider</h3>
            </div>
              <div class="box-body">
			  
                <div class="form-group col-md-4">
                  <label for="sli_text1" class="col-sm-12 control-label">Text 1</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="sli_text1">
                  </div>
                </div>
				
				<div class="form-group col-md-4">
                  <label for="sli_text2" class="col-sm-12 control-label">Text 2</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="sli_text2">
                  </div>
                </div>
						
				<div class="form-group col-md-12">
                  <label for="Content" class="col-sm-12 control-label">Image (Picture otomatis di resize ke 800x520px)</label>
                  <div class="col-sm-12">
                    <input type="file" name="file" id="file" /> 
                  </div>
                </div>

              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <a href="dashboard.php" class="btn btn-default">Reset</a>
			  </div>
			  
          </div>
        </div>
        </form>
		<?php } ?>
		<!-- /tambah slider -->
		
		<!-- Modifikasi slider -->
		<?php
		if($id != ""){
		$editHomeQ = mysqli_query($con, "select * FROM ms_slider WHERE sli_id = $id");
		$editHome = mysqli_fetch_array($editHomeQ);
		?>
		<form enctype="multipart/form-data" action="scripts/slider.php" method="post">
        <input type="hidden" value="homepageslider" name="hiddentype" />
		<input type="hidden" value="<?php echo $id; ?>" name="id" />
		<div class="col-md-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Slider</h3>
            </div>
			
              <div class="box-body">
			  
                <div class="form-group col-md-4">
                  <label for="sli_text1" class="col-sm-12 control-label">Text 1</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="sli_text1" value="<?php echo $editHome['sli_text1']; ?>">
                  </div>
                </div>
				
				<div class="form-group col-md-4">
                  <label for="sli_text2" class="col-sm-12 control-label">Text 2</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="sli_text2" value="<?php echo $editHome['sli_text2']; ?>">
                  </div>
                </div>
						
				<div class="form-group col-md-12">
                  <label for="Content" class="col-sm-12 control-label">Image (Picture otomatis di resize ke 800x520px)</label>
                  <div class="col-sm-12">
                    <input type="file" name="file" id="file" /> 
                  </div>
                </div>

              </div>
              
			  <div class="box-footer" style="background:#eee">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <a href="dashboard.php" class="btn btn-default">Reset</a>
			  </div>
			  
          </div>
        </div>
        </form>
		<?php } ?>
		
      </div>
    </section>
  </div>

  <?php include "base/footer.html"; ?>
</div>

<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/demo.js"></script>

<script>
$(function () {
	$('#example1').DataTable();
});
</script>
</body>
</html>