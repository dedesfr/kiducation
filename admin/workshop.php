<?php
session_start();
include("base/koneksi.php");
$page		= "workshop";
$pagetree	= "workshop";
date_default_timezone_set("Asia/Jakarta");

$idadmin = $_SESSION['idadmin'];
if($idadmin == ""){
	$_SESSION['error'] = "Silahkan login terlebih dahulu";
	header("location:index.php");
}
$namaOpr = $_SESSION['nama'];

$info = "";
$info = @$_GET['info'];

$id = "";
$id = @$_GET['id'];
$foodSelectQ = mysqli_query($con, "SELECT * FROM food WHERE status = 'active'");

$pastWorkshopQ = mysqli_query($con, "SELECT * FROM past_workshop WHERE workshop_id = $id");

$countPastWorkshop = 0;
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Kiducation Administrator</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="icon" href="base/favicon.png" type="image/x-icon">

	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
	<link rel="stylesheet" href="dist/css/ionicons-2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">
	<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
	<link rel="stylesheet" href="plugins/datetimepicker/css/bootstrap-datetimepicker.min.css">
	<script src="ckeditor/ckeditor.js"></script>
	<script src="ckeditor/config.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  
  <?php include "base/header.php"; ?>
  <?php include "base/sidebar.html"; ?>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>Workshop Editor<small><?php echo $info; ?></small></h1>
    </section>

    <section class="content">
      <div class="row">
	  
		<!-- Location -->
		<div class="col-md-12">
		  <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Content Workshop</h3>
            </div>
            <div class="box-body">
              <table id="example" class="table table-hover">
                <thead>
                <tr>
                  <th style="width:5%;">Action</th>
				  <th>Header</th>
                </tr>
                </thead>
                <tbody>
                <?php
				$workshopQ = mysqli_query($con, "select w_id, w_header FROM tr_workshop ORDER BY w_id DESC");
				while($workshop = mysqli_fetch_array($workshopQ)){
				?>
				<tr>
				  <td><a href="workshop.php?id=<?php echo $workshop['w_id']; ?>">Edit</a></td>
                  <td><?php echo $workshop['w_header']; ?></td>
                </tr>
                <?php } ?>
				</tbody>
              </table>
            </div>
          </div>
        </div>
		<!-- /Location -->
        
        <!-- add Content -->
		<?php if($id == ""){ ?>
		<form action="scripts/workshopEditor.php" method="post" enctype="multipart/form-data">
		<input type="hidden" value="x" name="id" />
		<div class="col-md-12">
		
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Content Workshop</h3>
            </div>
			
              <div class="box-body">
			  
                <div class="form-group col-md-12">
                  <label for="Header" class="col-sm-12 control-label">Header</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="Header">
                  </div>
                </div>

                <div class="form-group col-md-12">
                  <label for="SubHeader" class="col-sm-12 control-label">SubHeader</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="SubHeader">
                  </div>
                </div>
				
                <div class="form-group col-md-3">
                  <label for="Tanggal" class="col-sm-12 control-label">Start Date</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control input-append date" name="Tanggal" id="addTgl">
                  </div>
                </div>

                <div class="form-group col-md-3">
                  <label for="EndTanggal" class="col-sm-12 control-label">End Date</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control input-append date" name="EndTanggal" id="addEndTgl">
                  </div>
                </div>

                <div class="form-group col-md-3">
                  <label for="EndRegis" class="col-sm-12 control-label">End Date Registration</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control input-append date" name="EndRegis" id="addEndRegis">
                  </div>
                </div>

                <div class="clearfix"></div>
                <div class="form-group col-md-6">
                  <label for="Address" class="col-sm-12 control-label">Address</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="Address">
                  </div>
                </div>

                <div class="form-group col-md-3">
                  <label for="Lat" class="col-sm-12 control-label">Lat</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="Lat">
                  </div>
                </div>

                <div class="form-group col-md-3">
                  <label for="Len" class="col-sm-12 control-label">Len</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="Len">
                  </div>
                </div>

                <div class="form-group col-md-2">
                  <label for="ClassSize" class="col-sm-12 control-label">Class Size</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="ClassSize">
                  </div>
                </div>

                <div class="form-group col-md-2">
                  <label for="MinAge" class="col-sm-12 control-label">Minimum Age</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="MinAge">
                  </div>
                </div>

                <div class="form-group col-md-2">
                  <label for="Level" class="col-sm-12 control-label">Level</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="Level">
                  </div>
                </div>

                <div class="form-group col-md-2">
                  <label for="Language" class="col-sm-12 control-label">Language</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="Language">
                  </div>
                </div>

                <div class="form-group col-md-2">
                  <label for="Price" class="col-sm-12 control-label">Price</label>
                  <div class="col-sm-12">
                    <input type="number" class="form-control" name="Price">
                  </div>
                </div>
                
                
				        <div class="form-group col-md-12">
                  <label for="Content" class="col-sm-12 control-label">Image</label>
                  <div class="col-sm-12">
                    <input type="file" name="file" id="file" /> 
                  </div>
                </div>
				
                <div class="form-group col-md-12">
                  <label for="Description" class="col-sm-12 control-label">Content</label>
                  <div class="col-sm-12">
                    <textarea name="wContent" id="editor2" rows="10" cols="80"></textarea>
                  </div>
                </div>

                <div class="form-group col-md-12">
                  <label for="FoodSelect" class="col-sm-12 control-label">Food</label>
                  <div class="col-sm-12">
                    <?php while($foodSelectResult = mysqli_fetch_assoc($foodSelectQ)):; ?>
                      <input type = "checkbox" name= "foodSelect[]" value ="<?php echo $foodSelectResult['id'] ?>"> <?php echo $foodSelectResult['nama'] ?> <br/>
                    <?php endwhile;?>
                  </div>
                </div>

              </div>
			  
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <a href="workshop.php" class="btn btn-default">Reset</a>
			  </div>
			  
          </div>
        </div>
        </form>
		<?php } ?>
		<!-- /add Content -->
		
		<!-- Modifikasi Content -->
		<?php
		if($id != ""){
		$editWorkshopQ = mysqli_query($con, "select * FROM tr_workshop WHERE w_id = $id");
		$editWorkshop = mysqli_fetch_array($editWorkshopQ);
		$date = new DateTime($editWorkshop['w_date']);
		$date =  $date->format('m/d/Y');
		$date2 = new DateTime($editWorkshop['w_endDate']);
		$date2 =  $date2->format('m/d/Y');
		$date3 = new DateTime($editWorkshop['w_lastDate']);
		$date3 =  $date3->format('m/d/Y');
		?>
		<form action="scripts/workshopEditor.php" method="post" enctype="multipart/form-data">
		<input type="hidden" value="<?php echo $id; ?>" name="id" />
		<div class="col-md-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Modifikasi Content Workshop</h3>
            </div>
              <div class="box-body">
			  
                <div class="form-group col-md-9">
                  <label for="Header" class="col-sm-12 control-label">Header</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="Header" value='<?php echo $editWorkshop['w_header']; ?>'>
                  </div>
                </div>

                <div class="form-group col-md-12">
                  <label for="SubHeader" class="col-sm-12 control-label">SubHeader</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="SubHeader" value='<?php echo $editWorkshop['w_subheader']; ?>'>
                  </div>
                </div>

                <div class="form-group col-md-3">
                  <label for="Tanggal" class="col-sm-12 control-label">Start Date</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control input-append date" name="Tanggal" id="editTgl" value='<?php echo $date; ?>'>
                  </div>
                </div>

                <div class="form-group col-md-3">
                  <label for="EndTanggal" class="col-sm-12 control-label">End Date</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control input-append date" name="EndTanggal" id="editEndTgl" value='<?php echo $date2; ?>'>
                  </div>
                </div>

                <div class="form-group col-md-3">
                  <label for="EndRegis" class="col-sm-12 control-label">End Date Registration</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control input-append date" name="EndRegis" id="editEndRegis" value='<?php echo $date3; ?>'>
                  </div>
                </div>

                <div class="clearfix"></div>
                <div class="form-group col-md-6">
                  <label for="Address" class="col-sm-12 control-label">Address</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="Address" value='<?php echo $editWorkshop['w_address']; ?>'>
                  </div>
                </div>

                <div class="form-group col-md-3">
                  <label for="Lat" class="col-sm-12 control-label">Lat</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="Lat" value='<?php echo $editWorkshop['w_lat']; ?>'>
                  </div>
                </div>

                <div class="form-group col-md-3">
                  <label for="Len" class="col-sm-12 control-label">Len</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="Len" value='<?php echo $editWorkshop['w_len']; ?>'>
                  </div>
                </div>

                <div class="form-group col-md-2">
                  <label for="ClassSize" class="col-sm-12 control-label">Class Size</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="ClassSize" value='<?php echo $editWorkshop['w_classSize']; ?>'>
                  </div>
                </div>

                <div class="form-group col-md-2">
                  <label for="MinAge" class="col-sm-12 control-label">Minimum Age</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="MinAge" value='<?php echo $editWorkshop['w_minAge']; ?>'>
                  </div>
                </div>

                <div class="form-group col-md-2">
                  <label for="Level" class="col-sm-12 control-label">Level</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="Level" value='<?php echo $editWorkshop['w_level']; ?>'>
                  </div>
                </div>

                <div class="form-group col-md-2">
                  <label for="Language" class="col-sm-12 control-label">Language</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="Language" value='<?php echo $editWorkshop['w_language']; ?>'>
                  </div>
                </div>

                <div class="form-group col-md-2">
                  <label for="Price" class="col-sm-12 control-label">Price</label>
                  <div class="col-sm-12">
                    <input type="number" class="form-control" name="Price" value='<?php echo $editWorkshop['w_price']; ?>'>
                  </div>
                </div>
				
				        <div class="form-group col-md-12">
                  <label for="Content" class="col-sm-12 control-label">Image (kosongkan jika tidak ingin diganti)</label>
                  <div class="col-sm-12">
                    <input type="file" name="file" id="file" /> 
                  </div>
                </div>
				
				        <div class="form-group col-md-12">
                  <label for="wContent" class="col-sm-12 control-label">Content</label>
                  <div class="col-sm-12">
                    <textarea name="wContent" id="editor" rows="10" cols="80"><?php echo $editWorkshop['w_content']; ?></textarea>
                  </div>
                </div>

				        <div class="form-group col-md-2">
                  <label for="wContent" class="col-sm-12 control-label">Show in Past Workshop?</label>
                  <div class="col-sm-12">
                    <select name="showInPastWorkshop">
                      <?php if ($editWorkshop['isShowPastWorkshop'] == "no") { ?>
                        <option value="no">No</option>
                        <option value="yes">Yes</option>
                      <?php } else { ?>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

				        <div class="form-group col-md-2">
                  <label for="wContent" class="col-sm-12 control-label">Workshop Status</label>
                  <div class="col-sm-12">
                    <select name="workshopStatus">
                      <?php if ($editWorkshop['status'] == "inactive") { ?>
                        <option value="inactive">Inactive</option>
                        <option value="active">Active</option>
                      <?php } else { ?>
                        <option value="active">Active</option>
                        <option value="inactive">Inactive</option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group col-md-12">
                  <label for="Content" class="col-sm-12 control-label">Image (buat past workshop)</label>
                  <div class="col-sm-12">
                    <input type="file" name="file_past[]" multiple /> 
                  </div>
                </div>

                <div class="form-group col-md-12">
                  <label for="Content" class="col-sm-12 control-label">Image (buat past workshop)</label>
                  <div class="table-responsiveaa">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Picture</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php while($pastWorkshopResult = mysqli_fetch_assoc($pastWorkshopQ)):; $countPastWorkshop += 1;?>
                          <tr>
                            <td><?php echo $countPastWorkshop ?></td>
                            <td><img src= <?php echo $pastWorkshopResult['images']?> alt="ga ada" width="300px" height="200px"></td>
                            <td><a data-id="<?php echo $pastWorkshopResult['id']; ?>" class="btn btn-danger delPast">Delete</a> </td>
                          </tr>
                        <?php endwhile;?>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="form-group col-md-2">
                  <label for="Level" class="col-sm-12 control-label">Youtube URL</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="Youtube" value='<?php echo $editWorkshop['w_youtubeUrl']; ?>'>
                  </div>
                </div>

              </div>
			  
              <div class="box-footer" style="background:#eee">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <a href="workshop.php" class="btn btn-default">Reset</a>
			  </div>
          </div>
        </div>
        </form>
		<?php } ?>
		
      </div>
    </section>
  </div>
  <?php include "base/footer.html"; ?>
</div>

<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/demo.js"></script>
<script src="../dist/sweetalert/sweetalert.min.js"></script>

<script>
$(function () {
	$('#example').DataTable();
	<?php if($id == "") { ?>
		CKEDITOR.replace( 'editor2', {
		height: 300,
		filebrowserUploadUrl: "scripts/upload_workshop.php",
	} );
	$( "#addTgl" ).datetimepicker({format: 'yyyy-mm-dd hh:ii'});
	$( "#addEndTgl" ).datetimepicker({format: 'yyyy-mm-dd hh:ii'});
	$( "#addEndRegis" ).datetimepicker({format: 'yyyy-mm-dd hh:ii'});
	<?php }else{ ?>
        CKEDITOR.replace( 'editor', {
		height: 300,
		filebrowserUploadUrl: "scripts/upload_workshop.php",
	} );
	$( "#editTgl" ).datetimepicker({format: 'yyyy-mm-dd hh:ii'});
	$( "#editEndTgl" ).datetimepicker({format: 'yyyy-mm-dd hh:ii'});
	$( "#editEndRegis" ).datetimepicker({format: 'yyyy-mm-dd hh:ii'});
	<?php } ?>

  $('.delPast').click(function (e) {
      var past_workshop_id = $(this).data('id');
      var dataPass = 'past_workshop_id=' + past_workshop_id
      var $ele = $(this).parent().parent();

      swal({
          title: "Are you sure?",
          text: "Order Approved Confirmation",
          icon: "info",
          buttons: true,
          })
          .then((willDelete) => {
          if (willDelete) {
              $.ajax({
                  type: "POST",
                  url: "delPast.php",
                  data: dataPass,
                  cache: false,
                  success: function(result){
                      swal("Poof! Image Deleted successfully.", {
                      icon: "success",
                      });
                      $ele.fadeOut(2000);
                  }
              });
          } else {
              swal("Cancelled");
          }
      });
      
  });
});
</script>
</body>
</html>