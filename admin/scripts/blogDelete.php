<?php
session_start();
include "../base/koneksi.php";

function start(){ mysqli_query($con, 'START TRANSACTION;'); }
function commit(){ mysqli_query($con, "COMMIT"); }
function rollback(){ mysqli_query($con, "ROLLBACK"); }

$id  	= @$_REQUEST['id'];
	
if($id == "")
	header("location:../blog.php?info= - ID tidak boleh kosong.");
else
{
	try
	{
		start();
		
		$delete = mysqli_query($con, "DELETE FROM tr_blog WHERE t_id = '$id'");
		if(!$delete)
			throw new Exception("gagal");

		commit();
		header("location:../blog.php?info= - Delete blog berhasil.");
	}
	catch(Exception $e){
		rollback();
		header("location:../blog.php?info= - Delete blog gagal.");
	}
}

mysqli_close($con);
?>