<?php
session_start();
include "../base/koneksi.php";

function start(){ mysqli_query($con, 'START TRANSACTION;'); }
function commit(){ mysqli_query($con, "COMMIT"); }
function rollback(){ mysqli_query($con, "ROLLBACK"); }

$id  	= @$_REQUEST['id'];
	
if($id == "")
	header("location:../workshop.php?info= - ID tidak boleh kosong.");
else
{
	try
	{
		start();
		
		$cekFileQuery 	= mysqli_query($con, "SELECT w_picture FROM tr_workshop WHERE w_id = '$id'");
		$cekFile 		= mysqli_fetch_array($cekFileQuery);
		$cekFilePic		= $cekFile['w_picture'];

		//unlink($cekFilePic);
		unlink("../../".$cekFilePic);
		
		$delete = mysqli_query($con, "DELETE FROM tr_workshop WHERE w_id = '$id'");
		if(!$delete)
			throw new Exception("gagal");

		commit();
		header("location:../workshop.php?info= - Delete workshop berhasil.");
	}
	catch(Exception $e){
		rollback();
		header("location:../workshop.php?info= - Delete workshop gagal.");
	}
}

mysqli_close($con);
?>