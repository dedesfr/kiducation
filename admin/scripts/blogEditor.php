<?php
session_start();
include "../base/koneksi.php";
include "SimpleImage.php";
date_default_timezone_set("Asia/Jakarta");

$hiddenType = @$_REQUEST['hiddentype'];
$id			= @$_REQUEST['id'];
$Header		= @$_REQUEST['Header'];
$SubHeader	= @$_REQUEST['SubHeader'];
$wContent	= @$_REQUEST['wContent'];

$Tanggal = new DateTime($_REQUEST['Tanggal']);
$Tanggal = $Tanggal->format('Y-m-d');

if($id == "")
	header("location:../blog.php?info= - ID tidak ditemukan");
else
{
	if($id == "x")
	{
		$filenyah 	= @$_FILES["file"]["name"];
		
		if($filenyah != "")
		{
			$allowedExts = array("jpg", "JPG", "jpeg", "png");
			$extension = end(explode(".", $_FILES["file"]["name"]));
			
			if ((($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/JPG") || ($_FILES["file"]["type"] == "image/png") || 
			($_FILES["file"]["type"] == "image/pjpeg")) && ($_FILES["file"]["size"] < 500000000) && in_array($extension, $allowedExts))
			{
				if ($_FILES["file"]["error"] > 0)
					header("location: ../blog.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 5MB.");
				else
				{
					$cekID = mysqli_fetch_array(mysqli_query($con, "SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '$ai' AND TABLE_NAME = 'tr_blog'")); 
					$curID = $cekID["AUTO_INCREMENT"];
							
					$UploadKemanaFilenya1 	= "../../gambar/";
					$UploadKemanaFilenya2 	= "gambar/";
					$fileName 				= "blog_".$curID.".".$extension;
					$tmpFile				= $_FILES["file"]["tmp_name"];
					$filePath 				= $UploadKemanaFilenya1.$fileName;
					$filePath3				= $UploadKemanaFilenya2.$fileName;
					
					$simpen = move_uploaded_file($tmpFile, $filePath);	  
					if($simpen == 1)
					{						
						$insert = mysqli_query($con, "INSERT INTO tr_blog (t_header, t_subheader, t_date, t_content, t_picture) 
						VALUES ('$Header', '$SubHeader', '$Tanggal', '$wContent', '$filePath3')");
						if($insert == 1)
							header("location: ../blog.php?info= - Content sukses ditambah");
					}
					else
					  header("location: ../blog.php?info= - Content gagal ditambah");
				}
			}
			else
				header("location: ../blog.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 5MB.");
		}
		else
			header("location: ../blog.php?info= - Image harus diisi.");
	}
	else
	{
		$filenyah 	= @$_FILES["file"]["name"];
	
		if($filenyah != "")
		{
			$allowedExts = array("jpg", "jpeg", "png");
			$extension = end(explode(".", $_FILES["file"]["name"]));
			
			if ((($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/pjpeg")) && ($_FILES["file"]["size"] < 500000000) && in_array($extension, $allowedExts))
			{
				if ($_FILES["file"]["error"] > 0)
					header("location: ../blog.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 5MB.");
				else
				{			
					$UploadKemanaFilenya1 	= "../../gambar/";
					$UploadKemanaFilenya2 	= "gambar/";
					$fileName 				= "blog_".$id.".".$extension;
					$tmpFile				= $_FILES["file"]["tmp_name"];
					$filePath 				= $UploadKemanaFilenya1.$fileName;
					$filePath3				= $UploadKemanaFilenya2.$fileName;
					
					$simpen = move_uploaded_file($tmpFile, $filePath);	  
					if($simpen == 1)
					{						
						$update = mysqli_query($con, "UPDATE tr_blog SET t_header = '$Header', t_subheader = '$SubHeader', t_date = '$Tanggal', t_content = '$wContent', t_picture = '$filePath3' 
						WHERE t_id = '$id'");
						if($update == 1)
							header("location: ../blog.php?info= - blog sukses diupdate");
					}
					else
					  header("location: ../blog.php?info= - blog gagal diupdate");
				}
			}
			else
				header("location: ../blog.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 5MB.");
		}
		else
		{	
			$update = mysqli_query($con, "UPDATE tr_blog SET t_header = '$Header', t_subheader = '$SubHeader', t_date = '$Tanggal', t_content = '$wContent' WHERE t_id = '$id'");
			if($update == 1)
				header("location:../blog.php?info= - blog sukses diupdate");
			else
				header("location:../blog.php?info= - blog gagal diupdate");
		}
	}
}
mysqli_close($con);
?>