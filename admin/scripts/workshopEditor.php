<?php
session_start();
include "../base/koneksi.php";
date_default_timezone_set("Asia/Jakarta");

$hiddenType 		= @$_REQUEST['hiddentype'];
$id					= @$_REQUEST['id'];
$Header				= @$_REQUEST['Header'];
$SubHeader			= @$_REQUEST['SubHeader'];
$Address			= @$_REQUEST['Address'];
$Lat				= @$_REQUEST['Lat'];
$Len				= @$_REQUEST['Len'];
$ClassSize			= @$_REQUEST['ClassSize'];
$MinAge				= @$_REQUEST['MinAge'];
$Level				= @$_REQUEST['Level'];
$Language			= @$_REQUEST['Language'];
$Price				= @$_REQUEST['Price'];
$wContent			= @$_REQUEST['wContent'];
$foodSelect			= @$_REQUEST['foodSelect'];
$youtubeUrl			= @$_REQUEST['Youtube'];
$showInPastWorkshop = @$_REQUEST['showInPastWorkshop'];
$workshopStatus 	= @$_REQUEST['workshopStatus'];

$Tanggal = new DateTime($_REQUEST['Tanggal']);
$Tanggal = $Tanggal->format('Y-m-d H:i:s');
$Tanggal2 = new DateTime($_REQUEST['EndTanggal']);
$Tanggal2 = $Tanggal2->format('Y-m-d H:i:s');
$Tanggal3 = new DateTime($_REQUEST['EndRegis']);
$Tanggal3 = $Tanggal3->format('Y-m-d H:i:s');

if($id == "")
	header("location:../workshop.php?info= - ID tidak ditemukan");
else
{
	if($id == "x")
	{
		$filenyah 	= @$_FILES["file"]["name"];
		
		if($filenyah != "")
		{
			$allowedExts = array("jpg", "JPG", "jpeg", "png");
			$extension = end(explode(".", $_FILES["file"]["name"]));
			$rand = substr(md5(uniqid(rand(),1)),3,5);
			$fileNameTrim = str_replace(' ','',$_FILES["file"]["name"]);
			
			if ((($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/JPG") || ($_FILES["file"]["type"] == "image/png") || 
			($_FILES["file"]["type"] == "image/pjpeg")) && ($_FILES["file"]["size"] < 500000000) && in_array($extension, $allowedExts))
			{
				if ($_FILES["file"]["error"] > 0)
					header("location: ../workshop.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 5MB.");
				else
				{
					$cekID = mysqli_fetch_array(mysqli_query($con, "SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '$ai' AND TABLE_NAME = 'tr_workshop'")); 
					$curID = $cekID["AUTO_INCREMENT"];
							
					$UploadKemanaFilenya1 	= "../../gambar/";
					$UploadKemanaFilenya2 	= "gambar/";
					$fileName 				= "workshop_".$rand."-".$curID.".".$extension;
					$tmpFile				= $_FILES["file"]["tmp_name"];
					$filePath 				= $UploadKemanaFilenya1.$fileName;
					$filePath3				= $UploadKemanaFilenya2.$fileName;
					
					$simpen = move_uploaded_file($tmpFile, $filePath);	  
					if($simpen == 1)
					{						
						$insert = mysqli_query($con, "INSERT INTO tr_workshop (w_header, w_subheader, w_date, w_endDate, w_address, w_lat, w_len, w_classSize, w_minAge, w_level, w_language, w_price, w_lastDate, w_content, w_picture, isShowPastWorkshop, status) 
						VALUES ('$Header', '$SubHeader', '$Tanggal', '$Tanggal2', '$Address', '$Lat', '$Len', '$ClassSize', '$MinAge', '$Level', '$Language', '$Price', '$Tanggal3', '$wContent', '$filePath3', 'no', 'active')");
						if($insert == 1) {
							$last_workshop_id = $con->insert_id;
							for ($i = 0; $i <count($foodSelect);$i++){
								if(!empty($foodSelect)){
									$query = mysqli_query($con, "INSERT INTO workshop_food(workshop_id, food_id) VALUES ($last_workshop_id ,'".$foodSelect[$i]."')");
								}
							}
							header("location: ../workshop.php?info= - Content sukses ditambah");
						}
					}
					else
					  header("location: ../workshop.php?info= - Content gagal ditambah");
				}
			}
			else
				header("location: ../workshop.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 5MB.");
		}
		else
			header("location: ../workshop.php?info= - Image harus diisi.");
	}
	else
	{
		$filenyah 	= @$_FILES["file"]["name"];
	
		if($filenyah != "")
		{
			$allowedExts = array("jpg", "jpeg", "png");
			$extension = end(explode(".", $_FILES["file"]["name"]));
			$rand = substr(md5(uniqid(rand(),1)),3,5);
			
			if ((($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/pjpeg")) && ($_FILES["file"]["size"] < 500000000) && in_array($extension, $allowedExts))
			{
				if ($_FILES["file"]["error"] > 0)
					header("location: ../workshop.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 5MB.");
				else
				{			
					$UploadKemanaFilenya1 	= "../../gambar/";
					$UploadKemanaFilenya2 	= "gambar/";
					$fileName 				= "workshop_".$rand."-".$id.".".$extension;
					$tmpFile				= $_FILES["file"]["tmp_name"];
					$filePath 				= $UploadKemanaFilenya1.$fileName;
					$filePath3				= $UploadKemanaFilenya2.$fileName;
					
					$simpen = move_uploaded_file($tmpFile, $filePath);
					if($simpen == 1)
					{						
						$update = mysqli_query($con, "UPDATE tr_workshop SET w_header = '$Header', w_subheader = '$SubHeader', w_date = '$Tanggal', w_endDate = '$Tanggal2', w_address = '$Address', w_lat = '$Lat', w_Len = '$Len', w_classSize = '$ClassSize', w_minAge = '$MinAge', w_level = '$Level', w_language = '$Language', w_price = '$Price', w_lastDate = '$Tanggal3', w_content = '$wContent', w_picture = '$filePath3', w_youtubeUrl = '$youtubeUrl', isShowPastWorkshop = '$showInPastWorkshop', status = '$workshopStatus'
						WHERE w_id = '$id'");
						if($update == 1)
							header("location: ../workshop.php?info= - workshop sukses diupdate");
					}
					else
					  header("location: ../workshop.php?info= - workshop gagal diupdate");
				}
			}
			else
				header("location: ../workshop.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 5MB.");
		}
		else
		{
			for ($f=0; $f < count($_FILES["file_past"]["name"]); $f++) {
				$allowedExts = array("jpg", "jpeg", "png");
				$extension_temp = explode(".", $_FILES["file_past"]["name"][$f]);
				$extension = end($extension_temp);
				$rand = substr(md5(uniqid(rand(),1)),3,5);
				$fileNameTrim = str_replace(' ','',$_FILES["file_past"]["name"][$f]);
				if ((($_FILES["file_past"]["type"][$f] == "image/jpeg") || ($_FILES["file_past"]["type"][$f] == "image/png") || ($_FILES["file_past"]["type"][$f] == "image/pjpeg")) && ($_FILES["file_past"]["size"][$f] < 500000000) && in_array($extension, $allowedExts))
				{
					if ($_FILES["file_past"]["error"][$f] > 0) {
						header("location: ../workshop.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 5MB.");
					}  else {
						$UploadKemanaFilenya1 	= "../../gambar/past_workshop/";
						$UploadKemanaFilenya2 	= "gambar/past_workshop/";
						$fileName 				= "pastWorkshop_".$id."-"."$rand".$fileNameTrim;
						$tmpFile				= $_FILES["file_past"]["tmp_name"][$f];
						$filePath 				= $UploadKemanaFilenya1.$fileName;
						$filePath3				= $UploadKemanaFilenya2.$fileName;

						$simpen = move_uploaded_file($tmpFile, $filePath);
						if($simpen == 1)
						{				
							$insert = mysqli_query($con, "INSERT INTO past_workshop (workshop_id, images) 
							VALUES ('$id', '../$filePath3')");
							if($insert != 1) {
								header("location: ../workshop.php?info= - workshop gagal diupdate1");
							} else {
								header("location: ../workshop.php?info= - workshop berhasil diupdate");
							}
						}
						else {
							header("location: ../workshop.php?info= - workshop gagal diupdate2");
						}
					}
					
				} else {
					header("location: ../workshop.php?info= - File harus jpg, jpeg, atau png, dan ukuran kurang dari 5MB.");
				}
			}
		}
		$update = mysqli_query($con, "UPDATE tr_workshop SET w_header = '$Header', w_subheader = '$SubHeader', w_date = '$Tanggal', w_endDate = '$Tanggal2', w_address = '$Address', w_lat = '$Lat', w_Len = '$Len', w_classSize = '$ClassSize', w_minAge = '$MinAge', w_level = '$Level', w_language = '$Language', w_price = '$Price', w_lastDate = '$Tanggal3', w_content = '$wContent', w_youtubeUrl = '$youtubeUrl', isShowPastWorkshop = '$showInPastWorkshop', status = '$workshopStatus'
		WHERE w_id = '$id'");
		if($update == 1) {
			header("location: ../workshop.php?info= - workshop sukses diupdate");
		} else {
			header("location: ../workshop.php?info= - workshop gagal diupdate");
		}
	}
}
mysqli_close($con);
?>