<?php
session_start();
include("base/koneksi.php");
$page		= "blog";
$pagetree	= "blog";
date_default_timezone_set("Asia/Jakarta");

$idadmin = $_SESSION['idadmin'];
if($idadmin == ""){
	$_SESSION['error'] = "Silahkan login terlebih dahulu";
	header("location:index.php");
}
$namaOpr = $_SESSION['nama'];

$info = "";
$info = @$_GET['info'];

$id = "";
$id = @$_GET['id'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Kiducation Administrator</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="icon" href="base/favicon.png" type="image/x-icon">

	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
	<link rel="stylesheet" href="dist/css/ionicons-2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">
	<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
	<!-- <script src="http://cdn.ckeditor.com/4.6.2/full/ckeditor.js"></script> -->
  <script src="ckeditor/ckeditor.js"></script>
	<script src="ckeditor/config.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  
  <?php include "base/header.php"; ?>
  <?php include "base/sidebar.html"; ?>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>Blog Editor<small><?php echo $info; ?></small></h1>
    </section>

    <section class="content">
      <div class="row">
	  
		<!-- Location -->
		<div class="col-md-12">
		  <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Content Blog</h3>
            </div>
            <div class="box-body">
              <table id="example" class="table table-hover">
                <thead>
                <tr>
                  <th style="width:5%;">Action</th>
				  <th>Header</th>
				  <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php
				$blogQ = mysqli_query($con, "select t_id, t_header FROM tr_blog ORDER BY t_id DESC");
				while($blog = mysqli_fetch_array($blogQ)){
				?>
				<tr>
				  <td><a href="blog.php?id=<?php echo $blog['t_id']; ?>">Edit</a></td>
                  <td><?php echo $blog['t_header']; ?></td>
				  <td><a href="scripts/blogDelete.php?id=<?php echo $blog['t_id']; ?>">Delete</a></td>
                </tr>
                <?php } ?>
				</tbody>
              </table>
            </div>
          </div>
        </div>
		<!-- /Location -->
        
        <!-- add Content -->
		<?php if($id == ""){ ?>
		<form action="scripts/blogEditor.php" method="post" enctype="multipart/form-data">
		<input type="hidden" value="x" name="id" />
		<div class="col-md-12">
		
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Content Blog</h3>
            </div>
			
              <div class="box-body">
			  
                <div class="form-group col-md-9">
                  <label for="Header" class="col-sm-12 control-label">Header</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="Header">
                  </div>
                </div>

                <div class="form-group col-md-9">
                  <label for="SubHeader" class="col-sm-12 control-label">SubHeader</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="SubHeader">
                  </div>
                </div>
				
				<div class="form-group col-md-3">
                  <label for="Tanggal" class="col-sm-12 control-label">Date</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="Tanggal" id="addTgl">
                  </div>
                </div>
	
				<div class="form-group col-md-12">
                  <label for="Content" class="col-sm-12 control-label">Image</label>
                  <div class="col-sm-12">
                    <input type="file" name="file" id="file" /> 
                  </div>
                </div>
				
				<div class="form-group col-md-12">
                  <label for="Description" class="col-sm-12 control-label">Content</label>
                  <div class="col-sm-12">
                    <textarea name="wContent" id="editor2" rows="10" cols="80"></textarea>
                  </div>
                </div>

              </div>
			  
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <a href="blog.php" class="btn btn-default">Reset</a>
			  </div>
			  
          </div>
        </div>
        </form>
		<?php } ?>
		<!-- /add Content -->
		
		<!-- Modifikasi Content -->
		<?php
		if($id != ""){
		$editBlogQ = mysqli_query($con, "select * FROM tr_blog WHERE t_id = $id");
		$editBlog = mysqli_fetch_array($editBlogQ);
		$date = new DateTime($editBlog['t_date']);
		$date =  $date->format('m/d/Y');
		?>
		<form action="scripts/blogEditor.php" method="post" enctype="multipart/form-data">
		<input type="hidden" value="<?php echo $id; ?>" name="id" />
		<div class="col-md-8">
		
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Modifikasi Content Blog</h3>
            </div>
              <div class="box-body">
			  
                <div class="form-group col-md-9">
                  <label for="Header" class="col-sm-12 control-label">Header</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="Header" value='<?php echo $editBlog['t_header']; ?>'>
                  </div>
                </div>

                <div class="form-group col-md-9">
                  <label for="SubHeader" class="col-sm-12 control-label">SubHeader</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="SubHeader">
                  </div>
                </div>
				
				<div class="form-group col-md-3">
                  <label for="Tanggal" class="col-sm-12 control-label">Date</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="Tanggal" id="editTgl" value='<?php echo $date; ?>'>
                  </div>
                </div>
				
				<div class="form-group col-md-12">
                  <label for="Content" class="col-sm-12 control-label">Image (kosongkan jika tidak ingin diganti)</label>
                  <div class="col-sm-12">
                    <input type="file" name="file" id="file" /> 
                  </div>
                </div>
				
				<div class="form-group col-md-12">
                  <label for="wContent" class="col-sm-12 control-label">Content</label>
                  <div class="col-sm-12">
                    <textarea name="wContent" id="editor" rows="10" cols="80"><?php echo $editBlog['t_content']; ?></textarea>
                  </div>
                </div>

              </div>
			  
              <div class="box-footer" style="background:#eee">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <a href="blog.php" class="btn btn-default">Reset</a>
			  </div>
			  
          </div>
        </div>
        </form>
		<?php } ?>
		
      </div>
    </section>
  </div>
  <?php include "base/footer.html"; ?>
</div>

<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/demo.js"></script>

<script>
$(function () {
	$('#example').DataTable();
	<?php if($id == "") { ?>
		CKEDITOR.replace( 'editor2', {
		height: 300,
		filebrowserUploadUrl: "scripts/upload_blog.php",
	} );
	$( "#addTgl" ).datepicker({
	  dateFormat: "dd-mm-yy"
	});
	<?php }else{ ?>
        CKEDITOR.replace( 'editor', {
		height: 300,
		filebrowserUploadUrl: "scripts/upload_blog.php",
	} );
	$( "#editTgl" ).datepicker({
	  dateFormat: "dd-mm-yy"
	});
	<?php } ?>
});
</script>
</body>
</html>