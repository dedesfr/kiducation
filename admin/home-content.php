<?php
session_start();
include("base/koneksi.php");
$page 		= "home";
$pagetree	= "homecontent";

$idadmin = $_SESSION['idadmin'];

if($idadmin == ""){
	$_SESSION['error'] = "Silahkan login terlebih dahulu";
	header("location:index.php");
}
$namaOpr = $_SESSION['nama'];

$id = "";
$id = @$_GET['id'];

$info = "";
$info = @$_GET['info'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>DAB Administrator</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="icon" href="base/favicon.png" type="image/x-icon">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
	<link rel="stylesheet" href="dist/css/ionicons-2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">

	<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
	<script src="ckeditor/ckeditor.js"></script>
	<script src="ckeditor/config.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include "base/header.php"; ?>
  <?php include "base/sidebar.html"; ?>
  
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Homepage Content<small><?php echo $info; ?></small></h1>
    </section>

    <section class="content">
      <div class="row">
	  
		<!-- Content -->
		<div class="col-md-12">
		  <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Homepage Content <small>(refresh halaman ini jika Image belum berubah)</small></h3>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-hover">
                <thead>
                <tr>
                  <th>Judul</th>
                  <th>Sub Judul</th>
                  <th>Content</th>
                </tr>
                </thead>
                <tbody>
                <?php
				$homepageQ = mysqli_query($con, "select * FROM ms_home_content ORDER BY hc_id ASC");
				while($homepage = mysqli_fetch_array($homepageQ)){
				?>
				<tr>
                  <td><?php echo $homepage['judul']; ?></td>
                  <td><?php echo $homepage['sub_judul']; ?></td>
                  <td><?php echo $homepage['content']; ?></td>
                </tr>
                <?php } ?>
				</tbody>
              </table>
            </div>
          </div>
        </div>
		<!-- /home-content -->
        
		
		<!-- Modifikasi slider -->
		<?php
		$editHomeQ = mysqli_query($con, "select * FROM ms_home_content WHERE hc_id = 1");
		$editHome = mysqli_fetch_array($editHomeQ);
		?>
		<form enctype="multipart/form-data" action="scripts/home-content.php" method="post">
        <input type="hidden" value="homecontent" name="hiddentype" />
		<input type="hidden" value="<?php echo $id; ?>" name="id" />
		<div class="col-md-12">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Home Content</h3>
            </div>
			
              <div class="box-body">
			  
                <div class="form-group col-md-4">
                  <label for="judul" class="col-sm-12 control-label">Judul</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="judul" value="<?php echo $editHome['judul']; ?>">
                  </div>
                </div>
				
				<div class="form-group col-md-4">
                  <label for="sub_judul" class="col-sm-12 control-label">Sub Judul</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="sub_judul" value="<?php echo $editHome['sub_judul']; ?>">
                  </div>
                </div>
				
				<div class="form-group col-md-12">
                  <label for="content" class="col-sm-12 control-label">Long Description (untuk Halaman Home Content)</label>
                  <div class="col-sm-12">
                    <textarea name="content" id="editor1" rows="10" cols="80"><?php echo $editHome['content']; ?></textarea>
                  </div>
                </div>	

              </div>
              
			  <div class="box-footer" style="background:#eee">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <a href="home-content.php" class="btn btn-default">Reset</a>
			  </div>
			  
          </div>
        </div>
        </form>
		
      </div>
    </section>
  </div>

  <?php include "base/footer.html"; ?>
</div>

<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/demo.js"></script>

<script>
$(function () {
	$('#example1').DataTable();
  CKEDITOR.replace('editor1');
  CKEDITOR.replace('editor2');
});
</script>
</body>
</html>