<?php
session_start();
include("base/koneksi.php");
$page		= "headline";
$pagetree	= "headline";
date_default_timezone_set("Asia/Jakarta");

$idadmin = $_SESSION['idadmin'];
if($idadmin == ""){
	$_SESSION['error'] = "Silahkan login terlebih dahulu";
	header("location:index.php");
}
$namaOpr = $_SESSION['nama'];

$info = "";
$info = @$_GET['info'];

$id = "";
$id = @$_GET['id'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Kiducation Administrator</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="icon" href="base/favicon.png" type="image/x-icon">

	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
	<link rel="stylesheet" href="dist/css/ionicons-2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">
	<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
	<link rel="stylesheet" href="plugins/datetimepicker/css/bootstrap-datetimepicker.min.css">
	<script src="ckeditor/ckeditor.js"></script>
	<script src="ckeditor/config.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  
  <?php include "base/header.php"; ?>
  <?php include "base/sidebar.html"; ?>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>Headline Editor<small><?php echo $info; ?></small></h1>
    </section>

    <section class="content">
      <div class="row">
	  
		<!-- Location -->
		<div class="col-md-12">
		  <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Headline</h3>
            </div>
            <div class="box-body">
              <table id="example" class="table table-hover">
                <thead>
                <tr>
                  <th style="width:5%;">Action</th>
				  <th>Header</th>
                </tr>
                </thead>
                <tbody>
                <?php
				$headlineQ = mysqli_query($con, "select id, title FROM headline ORDER BY id DESC");
				while($headline = mysqli_fetch_array($headlineQ)){
				?>
				<tr>
				  <td><a href="headline.php?id=<?php echo $headline['id']; ?>">Edit</a></td>
                  <td><?php echo $headline['title']; ?></td>
                </tr>
                <?php } ?>
				</tbody>
              </table>
            </div>
          </div>
        </div>
		<!-- /Location -->
		
		<!-- Modifikasi Content -->
		<?php
		if($id != ""){
		$editHeadlineQ = mysqli_query($con, "select * FROM headline WHERE id = $id");
		$editHeadline = mysqli_fetch_array($editHeadlineQ);
		?>
		<form action="scripts/headlineEditor.php" method="post" enctype="multipart/form-data">
		<input type="hidden" value="<?php echo $id; ?>" name="id" />
		<div class="col-md-8">
		
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Modifikasi Headline</h3>
            </div>
              <div class="box-body">
			  
                <div class="form-group col-md-6">
                  <label for="Header" class="col-sm-12 control-label">Title</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="Title" value='<?php echo $editHeadline['title']; ?>'>
                  </div>
                </div>

              </div>
			  
              <div class="box-footer" style="background:#eee">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <a href="headline.php" class="btn btn-default">Reset</a>
			  </div>
			  
          </div>
        </div>
        </form>
		<?php } ?>
		
      </div>
    </section>
  </div>
  <?php include "base/footer.html"; ?>
</div>

<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/demo.js"></script>


</script>
</body>
</html>