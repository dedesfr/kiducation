<?php
session_start();
include("base/koneksi.php");
$page		= "food";
$pagetree	= "food";
date_default_timezone_set("Asia/Jakarta");

$idadmin = $_SESSION['idadmin'];
if($idadmin == ""){
	$_SESSION['error'] = "Silahkan login terlebih dahulu";
	header("location:index.php");
}
$namaOpr = $_SESSION['nama'];

$info = "";
$info = @$_GET['info'];

$id = "";
$id = @$_GET['id'];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Kiducation Administrator</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="icon" href="base/favicon.png" type="image/x-icon">

	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
	<link rel="stylesheet" href="dist/css/ionicons-2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">
	<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
	<link rel="stylesheet" href="plugins/datetimepicker/css/bootstrap-datetimepicker.min.css">
	<script src="ckeditor/ckeditor.js"></script>
	<script src="ckeditor/config.js"></script>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  
  <?php include "base/header.php"; ?>
  <?php include "base/sidebar.html"; ?>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>Food Editor<small><?php echo $info; ?></small></h1>
    </section>

    <section class="content">
      <div class="row">
	  
		<!-- Location -->
		<div class="col-md-12">
		  <div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Menu</h3>
            </div>
            <div class="box-body">
              <table id="example" class="table table-hover">
                <thead>
                <tr>
                  <th style="width:5%;">Action</th>
				  <th>Header</th>
                </tr>
                </thead>
                <tbody>
                <?php
				$foodQ = mysqli_query($con, "select id, nama FROM food ORDER BY id DESC");
				while($food = mysqli_fetch_array($foodQ)){
				?>
				<tr>
				  <td><a href="food.php?id=<?php echo $food['id']; ?>">Edit</a></td>
                  <td><?php echo $food['nama']; ?></td>
                </tr>
                <?php } ?>
				</tbody>
              </table>
            </div>
          </div>
        </div>
		<!-- /Location -->
        
        <!-- add Content -->
		<?php if($id == ""){ ?>
		<form action="scripts/foodEditor.php" method="post" enctype="multipart/form-data">
		<input type="hidden" value="x" name="id" />
		<div class="col-md-12">
		
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Menu</h3>
            </div>
			
              <div class="box-body">
			  
                <div class="form-group col-md-6">
                  <label for="Header" class="col-sm-12 control-label">Nama</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="Nama">
                  </div>
                </div>

                <div class="form-group col-md-6">
                  <label for="sel1">Status:</label>
                  <select class="form-control" id="sel1" name="Status">
                    <option value="active">Active</option>
                    <option value="inactive">Not Active</option>
                  </select>
                </div>

                <div class="form-group col-md-12">
                  <label for="Header" class="col-sm-12 control-label">Detail</label>
                  <div class="col-sm-12">
                    <textarea class="form-control" rows="5" id="comment" name="Detail"></textarea>
                  </div>
                </div>

              </div>
			  
              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <a href="food.php" class="btn btn-default">Reset</a>
			  </div>
			  
          </div>
        </div>
        </form>
		<?php } ?>
		<!-- /add Content -->
		
		<!-- Modifikasi Content -->
		<?php
		if($id != ""){
		$editFoodQ = mysqli_query($con, "select * FROM food WHERE id = $id");
		$editFood = mysqli_fetch_array($editFoodQ);
		?>
		<form action="scripts/foodEditor.php" method="post" enctype="multipart/form-data">
		<input type="hidden" value="<?php echo $id; ?>" name="id" />
		<div class="col-md-8">
		
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Modifikasi Menu</h3>
            </div>
              <div class="box-body">
			  
              <div class="form-group col-md-6">
                  <label for="Header" class="col-sm-12 control-label">Nama</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control" name="Nama" value='<?php echo $editFood['nama']; ?>'>
                  </div>
                </div>

                <div class="form-group col-md-6">
                  <label for="sel1">Status:</label>
                  <select class="form-control" id="sel1" name="Status">
                    <?php if ($editFood['status'] == "inactive") { ?>
                      <option value="inactive">Not Active</option>
                      <option value="active">Active</option>
                    <?php } else { ?>
                      <option value="active">Active</option>
                      <option value="inactive">Not Active</option>
                    <?php } ?>
                  </select>
                </div>

                <div class="form-group col-md-12">
                  <label for="Header" class="col-sm-12 control-label">Detail</label>
                  <div class="col-sm-12">
                    <textarea class="form-control" rows="5" name="Detail"><?php echo $editFood['detail']; ?></textarea>
                  </div>
                </div>

              </div>
			  
              <div class="box-footer" style="background:#eee">
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                <a href="food.php" class="btn btn-default">Reset</a>
			  </div>
			  
          </div>
        </div>
        </form>
		<?php } ?>
		
      </div>
    </section>
  </div>
  <?php include "base/footer.html"; ?>
</div>

<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="plugins/fastclick/fastclick.js"></script>
<script src="dist/js/app.min.js"></script>
<script src="dist/js/demo.js"></script>


</script>
</body>
</html>