<?php
session_start();
include("base/koneksi.php");

$err = "";
$err = @$_SESSION['error'];

$idUser = @$_SESSION['idUser'];
if($idUser != ""){
  header("location:index.php");
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php @include("partial/head.php") ?>
    <!-- <link rel="stylesheet" href="stylesheets/login.css"> -->

</head>
<body>
<?php @include("partial/navbar.php") ?>
  
  <div class="kiducation" style="position: relative;">
    <img alt="Logo Kiducation" class="pattern-move" id="pattern" src="images/pattern3.png" style="position: absolute; top: 90px; left: 25px;" width="150" />
    <img alt="Logo Kiducation" class="pattern-hover" id="pattern" src="images/pattern17.png" style="position: absolute; top: 90px; right:25px;" width="150" />
    <img alt="Logo Kiducation" id="pattern" src="images/pattern4.png" style="position: absolute; top: 49em; right: 26px;" width="150" />
    <div class="account py-main" >
      <h1 style="text-align: center; color: #4988cd; margin-bottom: 2rem;">Sign Up</h1>
      <div class="container container-sm">
        <div class="card shadow" style="background-color: #4988cd; color: #fff; position: relative; padding: 40px;">
          <img alt="Logo Kiducation" id="pattern" src="images/pattern2.png" style="position: absolute; right: -52px; top: -116px; transform: rotate(90deg);" width="200" />
          <img alt="Logo Kiducation" id="pattern" src="images/pattern2.png" style="position: absolute; left: -114px; bottom: -125px; transform: rotate(90deg);" width="200" />
          <p style="color: #fff; font-size: 15px; font-weight: 300; line-height: 2;">Hi Parents! Please make sure to sign up before registering to our workshop</p>
          <div class="card-body">
            <form action="php-scripts/register.php" method="post" class="form-signin">
              <div class="form-group">
                <label>Fullname :</label>
                <input type="text" id="Fullname" class="form-control" placeholder="Fullname" name="fullname" required>
              </div>
              <div class="form-group">
                <label>Email Address :</label>
                <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="email" required>
              </div>
              <div class="form-group">
                <label>Phone Number :</label>
                <input type="text" id="Fullname" class="form-control" placeholder="Phone Number" name="phone" required>
              </div>
              <div class="form-group">
                <label>Password :</label>
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>
              </div>
              <div class="form-group">
                <label>Retype Password :</label>
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="passwordVal" required>
              </div>
              <div class="checkbox">
                <label><input type="checkbox" name="subscribe" value="yes">I Agree to subsribe to Kiducation's newsletter and promotion email</label>
              </div>
              <span> <?php echo $err ?></span>
              <button class="btn btn-account btn-block" type="submit" style="width: 150px; margin: 0 auto;">sign up</button>
              <span>Already have a account? 
                <button class="btn btn-transparent" style="display: inline-block; width: auto; color: #e08361; font-weight: bold;">
                  <a href="login.php" style="color: #f3d847; font-size: 17px; padding-left: 3px;">Sign in</a>
                </button>
              </span>
            </form><!-- /form -->
          </div>
        </div><!-- /card-container -->
      </div><!-- /container -->
    </div>
  </div>

  <?php @include("partial/footer.php") ?>

    <div id="btn_login" class="modal" >
    
    <form class="modal-content animate" action="/action_page.php">
      <div class="container-fluid">
        <div class="card card-container">
          <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
          <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
          <p id="profile-name" class="profile-name-card"></p>
          <form class="form-signin">
              <span id="reauth-email" class="reauth-email"></span>
              <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
              <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
              <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
          </form><!-- /form -->
        </div><!-- /card-container -->
      </div><!-- /container -->
    </form>
    </div>

  <script>
    // Get the modal
    var modal_login = document.getElementById('btn_login');
    var modal_register = document.getElementById('btn_register');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal_login) {
            modal_login.style.display = "none";
        }
    }
    window.onclick = function(event) {
        if (event.target == modal_register) {
            modal_register.style.display = "none";
        }
    }
    </script>

    <?php @include("partial/script.php") ?>
</body>
</html>