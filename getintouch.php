<?php
session_start();
include("base/koneksi.php");

$idUser = $_SESSION['idUser'];
$email = $_SESSION['email'];
if(isset($_SESSION['fullname'])){
    $fullname = $_SESSION['fullname'];
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php @include("partial/head.php") ?>
</head>
<body>
<?php @include("partial/navbar.php") ?>
  
  <div class="kiducation">
    <div class="getintouch" style="position: relative;">
    <img alt="pattern" id="pattern" src="images/pattern2.png" style="position: absolute; top: 16em; left: 0;" width="200" />
    <img alt="pattern" class="pattern-move" id="pattern" src="images/pattern12.png" style="position: absolute; top: 16em; left: 80px;" width="100" />
    <img alt="pattern" id="pattern" src="images/pattern4.png" style="position: absolute; top: 60em; right: 30px;" width="150" />
      <div class="container">
        <div class="row">
          <div class="col-sm-3">
            <h1 class="hero-header" style="color: #4bc1c1;">get in touch</h1>
          </div>
          <div class="col-sm-9">
              <img src="images/blog-hero.jpg" class="img-responsive">
          </div>
        </div>
      </div>
    </div>

    <section class="py-main">
      <div class="container container-md">
        <div class="row">
          <div class="col-sm-4 animated fadeInUp delayp2">
            <h3>For any inquiries please contact us at</h3>
            <h4>Email: <a href="mailto: admin@kiducation.id">admin@kiducation.id</a></h4>
            <h4>Phone number: (62+) 85772121742</h4>
          </div>
          <div class="form-organize col-sm-8 animated fadeInUp delayp3">
              <label style="font-size: 18px; color: #3bb6bb; margin-bottom: 40px;">Contact Form</label>
              <form action="#" method="post" class="form-signin">
                <div class="form-group">
                  <label>Subject :</label>
                  <input type="text" id="Fullname" class="form-control" placeholder="Subject" name="subject" required>
                </div>
                <div class="form-group">
                  <label>Name :</label>
                  <input type="text" id="Fullname" class="form-control" placeholder="Fullname" name="email" required="">
                </div>
                <div class="form-group">
                  <label>Email :</label>
                  <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="phone" required>
                </div>
                <div class="form-group">
                  <label>Phone Number :</label>
                  <input type="text" id="inputPassword" class="form-control" placeholder="Phone Number" name="password" required="">
                </div>
                <div class="form-group">
                  <label>Message :</label>
                  <textarea class="form-control" rows="5" id="comment" required style="resize: none;"></textarea>
                </div>
                <button class="btn-register" type="submit">Send Message</button>
              </form><!-- /form -->
            </div>
        </div>
      </div>
    </section>
  </div>

  <?php @include("partial/footer.php") ?>

  <script>
    // Get the modal
    var modal_login = document.getElementById('btn_login');
    var modal_register = document.getElementById('btn_register');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal_login) {
            modal_login.style.display = "none";
        }
    }
    window.onclick = function(event) {
        if (event.target == modal_register) {
            modal_register.style.display = "none";
        }
    }
    </script>
    <!-- <?php @include("partial/head.php") ?> -->
</body>
</html>