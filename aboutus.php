<?php
session_start();
include("base/koneksi.php");

if(isset($_SESSION['fullname'])){
  $idUser = $_SESSION['idUser'];
  $email = $_SESSION['email'];
  $fullname = $_SESSION['fullname'];
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php @include("partial/head.php") ?>
</head>
<body>

<?php @include("partial/navbar.php") ?>
  
  <div class="kiducation">
    <div class="aboutus py-main" style="position: relative;">
      <img alt="pattern" id="pattern" src="images/pattern14.png" style="position: absolute; bottom: -28px; left: 0;" width="200" />
      <img alt="pattern" class="pattern-move" id="pattern" src="images/pattern15.png" style="position: absolute; bottom: -28px; left: 0;" width="200" />
            <img alt="pattern" id="pattern" src="images/pattern12.png" style="position: absolute; bottom: 0px; left: 0;" width="100" />
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-xs-12">
            <h1 class="hero-title-aboutus animated fadeInUp delayp1">ABOUT US</h1>
            <h4 class="hero-subtitle-aboutus animated fadeInUp delayp2" style="margin-top: 60px;"><strong>Kiducation</strong> is a children workshop organizer based in BSD City, Tangerang, Banten. We specialize in arts and crafts activities as it is proved to be beneficial for children development. We, however, believe that learning should not be boring. Our workshops are supplemented with educational games, all wrapped up in exciting themes and storylines!</h4>
          </div>
          <div class="col-md-8 col-xs-12 animated fadeInUp delayp2" style="position: relative; background: url(images/aboutus.png) no-repeat center; background-size: cover; height: 45rem;" >
            <img alt="pattern" id="pattern" src="images/pattern13.png" style="position: absolute; top: -13px; right: 0;" width="200" />
            <!-- <img src="images/aboutus.jpg" class="img-responsive"/> -->
          </div>
        </div>
      </div>
    </div>

    <div class="aboutus-whatweoffer">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2">
            <div class="col-sm-12">
              <h1 class="aboutus-header animated fadeInUp delayp1">WHAT WE OFFER</h1>
            </div>
            <div class="row animated fadeInUp delayp2">
              <div class="col-xs-12 col-sm-2 ">
                <h1 class="step-number">1</h1>
                <h5 class="step-number-text"><strong>Fun arts and crafts workshops</strong> <br><br><span style="font-weight: 300; font-size: 11px;">In English or Bahasa Indonesia</span></h5>
              </div>
              <div class="col-xs-12 col-sm-10">
                <div class="aboutus-content" style="color: #4c6e9c; font-size: 22px; padding-top: 37px;">Fun arts and crafts workshop for your kids held on Sunday morning at The Barn Restaurant, BSD, Tangerang.</div>
              </div>
            </div>
            <div class="row animated fadeInUp delayp3">
              <div class="col-xs-12 col-sm-2">
                <h1 class="step-number" style="margin-top: 30px;">2</h1>
                <h5 class="step-number-text"><strong>Workshop partnership</strong> <br><br><span style="font-weight: 300; font-size: 11px;">In English or Bahasa Indonesia</span></h5>
              </div>
              <div class="col-xs-12 col-sm-10">
                <div class="aboutus-content" style="color: #4c6e9c; font-size: 22px; margin-top: 30px;">Ready to partner with you to organize exciting arts and crafts activities, tailored to your preferences and needs. Available for birthday parties, school activities, community events, and playdates.</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="aboutus-aboutkidu" style="position: relative;">
      <img alt="pattern" id="pattern" src="images/pattern2.png" style="position: absolute; right: 50px; top: -140px; transform: rotate(90deg);" width="200" />
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2">
            <div class="col-sm-12">
              <h1 class="aboutus-header animated fadeInUp delayp3" style="margin-bottom: 30px">ABOUT KIDUCATION</h1>
            </div>
            <div class="col-sm-12 animated fadeInUp delayp4">
              <div class="aboutus-content" style="color: #fff; font-size: 18px;">Kiducation is a children workshop organizer based in BSD City, Tangerang, Banten. We specialize in arts and crafts activities as it is proved to be beneficial for children development. We, however, believe that learning should not be boring. Our workshops are supplemented with educational games, all wrapped up in exciting themes and storylines!</div><br>
              <div class="aboutus-content" style="color: #fff; font-size: 18px;">We develop our themes and storylines – which later are used as the concept for handicrafts and games – based on subjects that are fun for children to work with, such as character-based themes, popular events, and national holiday. In this way, children could learn not only about crafts, but also know more about the things that are happening around them. And of course, we do it in a fun way</div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="aboutus-learn" style="position: relative;">
      <img alt="pattern" class="pattern-move pattern-hover" id="pattern" src="images/pattern8.png" style="position: absolute; left: 0; top: 15em; " width="100" />
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-2">
            <div class="col-sm-12">
              <h1 class="aboutus-header animated fadeInUp delayp6" style="margin-bottom: 30px">WHAT WILL CHILDREN LEARN WITH KIDUCATION?</h1>
              <h4 class="col-sm-10 col-sm-offset-1 animated fadeInUp delayp2" style="color: #104c8b; margin-bottom: 40px; text-align: justify;">Our workshops are intended to help children development by improving The Three Domains of Educational Activities or Learning (Bloom’s Taxonomy).</h4>
            </div>
            <div class="col-sm-12 animated fadeInUp delayp3">
              <div class="col-sm-4">
                <h3 class="learn-title">COGNITIVE</h3>
                <div class="learn-subtitle">(mental skills or knowledge)</div>
                <div class="learn-subtitle">By participating in educational games, gaining new knowledge about the subjects, and making crafts.</div>
              </div>
              <div class="col-sm-4">
                <h3 class="learn-title">PSYCHOMOTOR</h3>
                <div class="learn-subtitle">(physical skills)</div>
                <div class="learn-subtitle">By learning how to crafts.</div>
              </div>
              <div class="col-sm-4">
                <h3 class="learn-title">AFFECTIVE</h3>
                <div class="learn-subtitle">(feelings or emotional areas)</div>
                <div class="learn-subtitle">By being encouraged to be confident, socialize, and respect each other.</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php @include("partial/footer.php") ?>

  <script>
    // Get the modal
    var modal_login = document.getElementById('btn_login');
    var modal_register = document.getElementById('btn_register');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal_login) {
            modal_login.style.display = "none";
        }
    }
    window.onclick = function(event) {
        if (event.target == modal_register) {
            modal_register.style.display = "none";
        }
    }
    </script>

    <?php @include("partial/script.php") ?>
    
</body>
</html>