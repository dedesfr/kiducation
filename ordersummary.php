<?php
session_start();
include("base/koneksi.php");

if(isset($_SESSION['fullname'])){
  $fullname = $_SESSION['fullname'];
  $idUser = $_SESSION['idUser'];
  $email = $_SESSION['email'];
  $phone = $_SESSION['phone'];
}
if (isset($_POST['submit'])) {
  
  $_SESSION['nama'] = $_POST['nama'];
  $_SESSION['umur'] = $_POST['umur'];
  $_SESSION['notes'] = $_POST['notes'];
  $_SESSION['food'] = $_POST['food'];
  $_SESSION['workshop_id'] = $_POST['workshop_id'];

  $nama = $_SESSION['nama'];
  $umur = $_SESSION['umur'];
  $notes = $_SESSION['notes'];
  $food = $_SESSION['food'];
  $workshop_id = $_SESSION['workshop_id'];
} else {
  header("location:activities.php");
}
if ($_SESSION['voucher_price_ses'] != null) {
  $voucher = $_SESSION['voucher_price_ses'];
  $voucher_id = $_SESSION['voucher_id_ses'];
}

$query = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM tr_workshop WHERE w_id = $workshop_id"));

$datee = new dateTime($query['w_date']); 
$datee = $datee->format('l, F j Y');
$datee3 = new dateTime($query['w_endDate']);
$datee3 = $datee3->format('g:ia');
$datee2 = new dateTime($query['w_date']); 
$datee2 = $datee2->format('g:ia');
$bundlePrice = 0;
$voucherPrice = 0;
$firstBuyPrice = 0;
$firstBuyPriceTotal = 0;
$bundleQ = mysqli_query($con, "SELECT * FROM bundle_discount");
$bundleResult = mysqli_fetch_array($bundleQ);
$bundlePrice = $bundleResult['price'];
$x = count($nama);
$y = $x/2;
$userQ = mysqli_query($con, "SELECT * FROM users WHERE id ='$idUser'");
$userResult = mysqli_fetch_array($userQ);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php @include("partial/head.php") ?>
</head>
<body>

<?php @include("partial/navbar.php") ?>
  
  <div class="kiducation" style="position: relative;">
    <img alt="Logo Kiducation" id="pattern" src="images/pattern3.png" style="position: absolute; top: 90px; left: 26em;" width="150" />
    <img alt="Logo Kiducation" id="pattern" src="images/pattern17.png" style="position: absolute; top: 100px; right:16em;" width="150" />
    <img alt="Logo Kiducation" id="pattern" src="images/pattern4.png" class="display-none-xs" style="position: absolute; top: 60em; right: 40px;" width="150" />
    <img alt="Logo Kiducation" id="pattern" src="images/pattern16.png" style="position: absolute; top: 30em; left: 50px;" width="250" />
    <img alt="Logo Kiducation" id="pattern" src="images/pattern3.png" style="position: absolute; bottom: 90px; left: 26em;" width="150" />
    <section class="py-main">
      <div class="container container-sm">
        <div class="ordersummary">
          <h3 class="text-center" style="color: #86cbba;">Order Summary</h3>
          <div class="row">
            <div class="col-sm-12"> 
              <div class="wrapper border-bottom" style="padding: 10px;">
                <table class="table">
                  <tbody>
                    <tr style="border-top-color: transparent;">
                      <td class="border-top-0 w-150">Nama Workshop</td>
                      <td class="border-top-0">:</td>
                      <td class="border-top-0"><?php echo $query['w_header'] ?></td>
                    </tr>
                    <tr>
                      <td class="border-top-0 w-150">Hari, Tanggal</td>
                      <td class="border-top-0">:</td>
                      <td class="border-top-0"><?php echo $datee?></td>
                    </tr>
                    <tr>
                      <td class="border-top-0 w-150">Jam Pelaksanaan</td>
                      <td class="border-top-0">:</td>
                      <td class="border-top-0"><?php echo $datee2 . " - " . $datee3?></td>
                    </tr>
                    <tr>
                      <td class="border-top-0 w-150">Lokasi Pelaksanaan</td>
                      <td class="border-top-0">:</td>
                      <td class="border-top-0"><?php echo $query['w_address'] ?></td>
                    </tr>
                    <tr>
                      <td class="border-top-0 w-150">Harga per Participant</td>
                      <td class="border-top-0">:</td>
                      <td class="border-top-0">Rp. <?php echo $query['w_price'] ?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="wrapper border-bottom" style="padding: 10px;">
                <h3 class="text-center text-underline mb-3" style="color: #86cbba;">Parent's Information</h3>
                <table class="table">
                  <tbody>
                    <tr style="border-top-color: transparent;">
                      <td class="border-top-0 w-150">Parent's Name</td>
                      <td class="border-top-0">:</td>
                      <td class="border-top-0"><?php echo $fullname ?></td>
                    </tr>
                    <tr>
                      <td class="border-top-0 w-150">Parent's Email</td>
                      <td class="border-top-0">:</td>
                      <td class="border-top-0"><?php echo $email ?></td>
                    </tr>
                    <tr>
                      <td class="border-top-0 w-150">Parent's Phone Number</td>
                      <td class="border-top-0">:</td>
                      <td class="border-top-0"><?php echo $phone ?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="wrapper border-bottom" style="padding: 10px;">
                <h3 class="text-center text-underline mb-3" style="color: #86cbba;">Registration Details</h3>
                <table class="table">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Participant Data</th>
                      <th>Registration Fee</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php for($x = 0; $x < count($nama); $x++){ ?>
                    <tr>
                      <td><?php echo $x+1 ?></td>
                      <td>
                        <p>Participant <?php echo $x+1;?></p>
                        <p>Name : <?php echo $nama[$x];?></p>
                        <p>Age: <?php echo $umur[$x];?></p>
                        <p>Lunch Menu:</p>
                        <p><?php $foodQ = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM food WHERE id = $food[$x]")); echo $foodQ['nama']?></p>
                      </td>
                      <td>Rp. <?php echo $query['w_price'] ?></td>
                    </tr>
                  <?php };?>
                  </tbody>
                  <tbody>
                    <tr>
                      <td></td>
                      <td>SubTotal</td>
                      <td>Rp. <?php echo $query['w_price'] * count($nama) ?></td>
                    </tr>
                    <?php if ($x > 1) {
                        if ($x%2 == 0) { 
                            $bundlePrice = $bundlePrice * $y; ?>
                            <tr>
                                <td></td>
                                <td>Bundle</td>
                                <td>Rp. <?php echo $bundlePrice ?></td>
                            </tr>
                        <?php } else { 
                            $bundlePrice = $bundlePrice * ($x-1)/2; ?>
                            <tr>
                                <td></td>
                                <td>Bundle</td>
                                <td>Rp. <?php echo $bundlePrice ?></td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                      <!-- #1 -->
                    <?php if ($_SESSION['voucher_price_ses'] != null) { ?>
                      <tr>
                          <td></td>
                          <td>Voucher</td>
                          <td>Rp. <?php $voucherQ = mysqli_fetch_array(mysqli_query($con, "SELECT * FROM voucher WHERE id = '$voucher_id'")); echo $voucherQ['name']?> <span>(<?php echo $voucherQ['price'] ?>)</span></td>
                      </tr>
                    <?php $voucherPrice = $voucherQ['price']; } ?>
                    <tr>
                      <td></td>
                      <td>Total</td>
                      <!-- #2 -->
                        <td>Rp. <?php echo $query['w_price'] * count($nama) - $bundlePrice - $voucherPrice ?></td>
                    </tr>
                  </tbody>
                  <!-- <p>total : </p> -->
                </table>
              </div>
            </div>
          </div>
        </div>
        <form method="post" action="checkout.php">
            <input type="hidden" name="total" value= <?php echo $query['w_price'] * count($nama) ?>>
            <input type="submit" name="submit" class="btn btn-register mt-3 float-right"/>
        </form>
      </div>
    </section>
  </div>

  <?php @include("partial/footer.php") ?>

  <script>
    // Get the modal
    var modal_login = document.getElementById('btn_login');
    var modal_register = document.getElementById('btn_register');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal_login) {
            modal_login.style.display = "none";
        }
    }
    window.onclick = function(event) {
        if (event.target == modal_register) {
            modal_register.style.display = "none";
        }
    }
    </script>

    <?php @include("partial/script.php") ?>
    
</body>
</html>