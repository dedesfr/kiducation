<?php
session_start();
include("base/koneksi.php");


if(isset($_SESSION['fullname'])){
  $fullname = $_SESSION['fullname'];
  $idUser = $_SESSION['idUser'];
  $email = $_SESSION['email'];
} else {
  $_SESSION['error'] = "Silahkan login terlebih dahulu";
	header("location:login.php");
}

$result = mysqli_query($con, "SELECT * FROM tr_order WHERE user_id = $idUser");
$result2 = mysqli_query($con, "SELECT * FROM tr_order WHERE user_id = $idUser");
$query2 = mysqli_fetch_array($result2); 

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php @include("partial/head.php") ?>
</head>
<body>

<?php @include("partial/navbar.php") ?>
  
  <div class="kiducation" style="position: relative;">
    <img alt="Logo Kiducation" id="pattern" src="images/pattern3.png" style="position: absolute; top: 61px; left: 10em;" width="150" />
    <!-- <img alt="Logo Kiducation" id="pattern" src="images/pattern4.png" class="display-none-xs" style="position: absolute; bottom: 0; right: 40px;" width="150" /> -->
    <section class="py-main">
      <div class="container">
        <div class="checkpayment">
          <h3 class="mb-3" style="color: #86cbba;">Check Order</h3>
          <div class="table-responsiveaa">
            <table class="table">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Total Payment</th>
                  <th>Payment Destination</th>
                  <th>Transaction Status</th>
                  <th>Action</th>
                  <th>Ticket</th>
                </tr>
              </thead>
              <tbody>
                <?php while($query = mysqli_fetch_assoc($result)):; ?>
                  <tr>
                    <td><?php echo date("l, j F Y") ?></td>
                    <td>Rp. <?php echo $query['total']?></td>
                    <td>BCA  6041436988 an Rainer Hartanto</td>
                    <td><?php echo $query['status']?></td>
                    <?php if ($query['attachment'] == null) { ?>
                      <form action="php-scripts/uploadAttachment.php" method="post" enctype="multipart/form-data">
                      <input type="hidden" value="<?php echo $query['id']; ?>" name="id" />
                      <td><input type="file" name="file" id="file" /> <input type="submit" name="submit" class="btn-outline"/></td>
                      <!-- <td></td> -->
                    <?php } else { ?>
                      <td><img src= <?php echo $query['attachment']?> alt="ga ada" width="150px" height="100px"></td>
                    <?php } ?>
                    </form>
                    <?php if ($query['status'] == "approved") { ?>
                      <form action="pdf.php" method="post">
                        <input type="hidden" name="checktype" value= <?php echo $query['id']?> />
                        <td><button type="submit" name="submit" class="btn-text"/>Download e-ticket</td>
                      </form>
                    <?php } else { ?>
                      <td>Order is not approved yet</td>
                    <?php } ?>
                  </tr>
                <?php endwhile;?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>
  </div>

  <?php @include("partial/footer.php") ?>

  <script>
    // Get the modal
    var modal_login = document.getElementById('btn_login');
    var modal_register = document.getElementById('btn_register');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal_login) {
            modal_login.style.display = "none";
        }
    }
    window.onclick = function(event) {
        if (event.target == modal_register) {
            modal_register.style.display = "none";
        }
    }
    </script>

    <?php @include("partial/script.php") ?>
    
</body>
</html>