<!DOCTYPE html>
<html lang="en">
<head>
    <?php @include("partial/head.php") ?>

</head>
<body>
<?php @include("partial/navbar.php") ?>
  
  <div class="kiducation" style="position: relative;">
    <img alt="Logo Kiducation" id="pattern" src="images/pattern3.png" style="position: absolute; top: 90px; left: 26em;" width="150" />
    <img alt="Logo Kiducation" id="pattern" src="images/pattern17.png" style="position: absolute; top: 150px; right:25em;" width="150" />
    <img alt="Logo Kiducation" id="pattern" src="images/pattern4.png" style="position: absolute; top: 30em; right: 26px;" width="150" />
    <img alt="Logo Kiducation" id="pattern" src="images/pattern16.png" style="position: absolute; top: 30em; left: 50px;" width="250" />
    <div class="login" style=" position: relative; padding-top: 50px; padding-bottom: 50px;">
      <h1 style="text-align: center; color: #4988cd; ">Checkout Berhasil</h1>
      <div class="container-fluid">
        <div class="card card-container card-account" style="background-color: #4988cd; position: relative;">
        <img alt="Logo Kiducation" id="pattern" src="images/pattern2.png" style="position: absolute; right: -52px; top: -116px; transform: rotate(90deg);" width="200" />
        <img alt="Logo Kiducation" id="pattern" src="images/pattern2.png" style="position: absolute; left: -126px; bottom: -113px; transform: rotate(90deg);" width="200" />
          <div class="alert alert-danger text-center">Pastikan Anda memasukan nomer rekening yang benar!</div>
          <p>Transfer pembayaran ke nomer rekening:</p>
          <p><img src="gambar/bca-logo.svg" style="width: 100px;"> 6030384324</p>
          <p>a/n PHILIPUS RAYMOND</p>
          <hr>
          <p>Jumlah yang dibayarkan</p>
          <h4 style="font-weight: bold;">Rp 200.000</h4>
          <br><br>
          <div class="alert alert-info">Kirim bukti pembayaran ke nomer ini</div> 

        </div><!-- /card-container -->
      </div><!-- /container -->
    </div>
  </div>

  <?php @include("partial/footer.php") ?>

  <script>
    // Get the modal
    var modal_login = document.getElementById('btn_login');
    var modal_register = document.getElementById('btn_register');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal_login) {
            modal_login.style.display = "none";
        }
    }
    window.onclick = function(event) {
        if (event.target == modal_register) {
            modal_register.style.display = "none";
        }
    }
    </script>
    <?php @include("partial/script.php") ?>
</body>
</html>
