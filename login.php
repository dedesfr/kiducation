<?php
session_start();
include_once "base/koneksi.php";

$idUser = @$_SESSION['idUser'];
if($idUser != ""){
header("location:index.php");
}
$err = "";
$err = @$_SESSION['error'];
/*
require_once __DIR__ . '/src/autoload.php';
$siteKey = '6LdeVxAUAAAAAJtKL2NopanvfzhbfPYn3_NkpeUg';
$secret = '6LdeVxAUAAAAADr0Lqr61rwfTPzRNSwfPMto02Mq';
$lang = 'en';
*/
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php @include("partial/head.php") ?>
    <!-- <link rel="stylesheet" href="stylesheets/login.css"> -->
</head>
<body>
<?php @include("partial/navbar.php") ?>
  
  <div class="kiducation" style="position: relative;">
    <img alt="Logo Kiducation" id="pattern" src="images/pattern3.png" style="position: absolute; top: 90px; left: 26em;" width="150" />
    <img alt="Logo Kiducation" id="pattern" src="images/pattern17.png" style="position: absolute; top: 150px; right:25em;" width="150" />
    <img alt="Logo Kiducation" id="pattern" src="images/pattern4.png" style="position: absolute; top: 30em; right: 26px;" width="150" />
    <img alt="Logo Kiducation" id="pattern" src="images/pattern16.png" style="position: absolute; top: 30em; left: 50px;" width="250" />
    <div class="login py-main">
      <div class="container container-xs">
        <h1 style="text-align: center; color: #4988cd; ">Log in</h1>
        <div class="card shadow" style="background-color: #4988cd; padding: 30px 20px; border-radius: 2px;">
          <!-- <img alt="Logo Kiducation" id="pattern" src="images/pattern2.png" style="position: absolute; right: -52px; top: -116px; transform: rotate(90deg);" width="200" />
          <img alt="Logo Kiducation" id="pattern" src="images/pattern2.png" style="position: absolute; left: -91px; bottom: -113px; transform: rotate(90deg);" width="200" /> -->
          <div class="card-body">
            <label style="color: #fff; font-size: 15px; font-weight: 300; line-height: 2;">Hi Parents! <br>Please make sure to sign in before registering to our workshop</label>
            <form action="php-scripts/login.php" method="post" class="form-signin">
              <div class="form-group">
                <span id="reauth-email" class="reauth-email"></span>
                <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
              </div>
              <div class="form-group">
                <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
              </div>
                <span style="color: #fff;"> <?php echo $err ?></span>
                <a href="forgotPassword.php" style="color: #Fff; margin-bottom: 1rem;" class="a-none">forgot password?</a>
                <button class="btn btn-block btn-success" type="submit">Sign in</button>
            </form><!-- /form -->
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php @include("partial/footer.php") ?>

  <script>
    // Get the modal
    var modal_login = document.getElementById('btn_login');
    var modal_register = document.getElementById('btn_register');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal_login) {
            modal_login.style.display = "none";
        }
    }
    window.onclick = function(event) {
        if (event.target == modal_register) {
            modal_register.style.display = "none";
        }
    }
    </script>
    <?php @include("partial/script.php") ?>
</body>
</html>