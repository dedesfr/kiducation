<?php
session_start();
include_once "base/koneksi.php";

$idUser = @$_SESSION['idUser'];
if($idUser != ""){
header("location:index.php");
}
$err = "";
$err = @$_SESSION['error'];
/*
require_once __DIR__ . '/src/autoload.php';
$siteKey = '6LdeVxAUAAAAAJtKL2NopanvfzhbfPYn3_NkpeUg';
$secret = '6LdeVxAUAAAAADr0Lqr61rwfTPzRNSwfPMto02Mq';
$lang = 'en';
*/
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php @include("partial/head.php") ?>
    <link rel="stylesheet" href="stylesheets/login.css">
</head>
<body>
<?php @include("partial/navbar.php") ?>
  
  <div class="kiducation" style="position: relative;">
    <img alt="Logo Kiducation" id="pattern" src="images/pattern3.png" style="position: absolute; top: 90px; left: 26em;" width="150" />
    <img alt="Logo Kiducation" id="pattern" src="images/pattern17.png" style="position: absolute; top: 150px; right:25em;" width="150" />
    <img alt="Logo Kiducation" id="pattern" src="images/pattern4.png" style="position: absolute; top: 30em; right: 26px;" width="150" />
    <img alt="Logo Kiducation" id="pattern" src="images/pattern16.png" style="position: absolute; top: 30em; left: 50px;" width="250" />
    <div class="login py-main">
      <div class="container container-xs">
        <h1 style="text-align: center; color: #4988cd; ">Forgot Password</h1>
        <div class="card card-account shadow-lg" style="background-color: #4988cd;">
          <!-- <img alt="Logo Kiducation" id="pattern" src="images/pattern2.png" style="position: absolute; right: -52px; top: -116px; transform: rotate(90deg);" width="200" />
          <img alt="Logo Kiducation" id="pattern" src="images/pattern2.png" style="position: absolute; left: -91px; bottom: -113px; transform: rotate(90deg);" width="200" /> -->
          <label style="color: #fff; font-size: 15px; font-weight: 300; line-height: 2;">Hi Parents! <br>Please make sure to sign in before registering to our workshop</label>
          <form action="php-scripts/forgotPassword.php" method="post" class="form-signin">
              <span id="reauth-email" class="reauth-email"></span>
              <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
              <span> <?php echo $err ?></span>
              <button class="btn-lg btn-success" type="submit">Send Password To My Email</button>
          </form><!-- /form -->
        </div>
      </div>
    </div>
  </div>

  <?php @include("partial/footer.php") ?>

  <script>
    // Get the modal
    var modal_login = document.getElementById('btn_login');
    var modal_register = document.getElementById('btn_register');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal_login) {
            modal_login.style.display = "none";
        }
    }
    window.onclick = function(event) {
        if (event.target == modal_register) {
            modal_register.style.display = "none";
        }
    }
    </script>
    <?php @include("partial/script.php") ?>
</body>
</html>